<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/aboutyou', 'HomeController@aboutyou')->name('aboutyou');
Route::post('/aboutyou', 'HomeController@aboutyoupost')->name('aboutyoupost');
Route::get('/yourflight', 'HomeController@yourflight')->name('yourflight');
Route::post('/yourflight', 'HomeController@yourflightpost')->name('yourflightpost');
Route::get('/review', 'HomeController@review')->name('review');
Route::post('/review', 'HomeController@reviewpost')->name('reviewpost');
Route::get('/thanks', 'HomeController@thanks')->name('thanks');
Route::post('/thanks', 'HomeController@thankspost')->name('thankspost');
Route::get('/faqs', 'HomeController@faqs')->name('faqs');
Route::get('/tandc', 'HomeController@tandc')->name('tandc');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('/aboutus', 'HomeController@aboutus')->name('aboutus');

//Campaign main page
Route::get('/campaigns', 'CampaignController@index')->name('campaigns');
//Create a new campaign record
Route::post('/campaigns', 'CampaignController@create')->name('campaign.create');
//Delete an existing record {campaign}
Route::delete('/campaigns/{campaign}', 'CampaignController@destroy')->name('campaign.delete');
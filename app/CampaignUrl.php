<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Kyslik\ColumnSortable\Sortable;

class CampaignUrl extends Model
{
    protected $connection = 'mysql2';
    //need to set table name otherwise assumed to be campaignUrls 
    protected $table = 'inspire-flights_campaign_urls'; 
    //need to set otherwise it is assumed that primary key is id
    protected $primaryKey = 'url';
    public $incrementing = false;
    protected $keyType = 'string';

    //Naming this function the same confuses 
    public function getcampaign()
    {
        return $this->belongsTo('App\Campaign', 'campaign', 'campaign');
    }
    //will not add created_at, update_at because they are not fields in our tables
    public $timestamps = false;
    //allows tables to be sortable (alongside pagination)
    use Sortable;
    public $sortable = ['url','cert','language','instruction'];

    public $fillable = ['url','cert','language','instruction'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon; 

class OfferFlight extends Model
{
    protected $connection = 'mysql2';
    //need to set table name otherwise assumed to be campaign 
    protected $table = 'inspire-flights_offer_flights'; 
    //need to set otherwise it is assumed that primary key is id
    protected $primaryKey = ['offer','sequence'];
    public $incrementing = false;
    protected $keyType = 'string';

    protected $dates = ['departure_date','arrival_date'];
    //will not add created_at, update_at because they are not fields in our tables
    public $timestamps = false;
    public function getoffer()
    {
        return $this->belongsTo('App\Offer', 'offer', 'offer');
    }
    public function setDepartureDateAttribute($value) {
        $this->attributes['departure_date'] = Carbon::createFromFormat('Y-m-d', $value)->timestamp;
    }  
    public function setArrivalDateAttribute($value) {
        $this->attributes['arrival_date'] = Carbon::createFromFormat('Y-m-d', $value)->timestamp;
    }  
    // public function getDateAttribute($value) {
    //     return Carbon::createFromFormat('U', $value)->format('d/m/Y');
    // }
}

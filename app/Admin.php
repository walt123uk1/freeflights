<?php

namespace App;

use Illuminate\Foundation\Auth\User as Model;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Notifications\Notifiable;

class Admin extends Model
{    
    use Notifiable;
    protected $connection = 'mysql2';
    protected $guarded = ['id'];
    //need to set table name otherwise assumed to be campaign 
    protected $table = 'inspire-flights_admin_logins'; 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getAuthPassword()
    {
     return $this->password;
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}

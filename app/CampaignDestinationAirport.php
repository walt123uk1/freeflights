<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class CampaignDestinationAirport extends Model
{
    protected $connection = 'mysql2';
    //need to set table name otherwise assumed to be campaignLang 
    protected $table = 'inspire-flights_campaign_destination_airports';
    //need to set otherwise it is assumed that primary key is id
    protected $primaryKey = ['campaign','departure','sequence'];
    public $incrementing = false;
    //Naming this function the same confuses 
    public function getcampaign()
    {
        return $this->belongsTo('App\Campaign', 'campaign', 'campaign');
    }
    public function getdepart()
    {
        return $this->belongsTo('App\CampaignDepartAirport', 'iata', 'departure');
    }
    //will not add created_at, update_at because they are not fields in our tables
    public $timestamps = false;
    //allows tables to be sortable (alongside pagination)
    use Sortable;
    public $sortable = ['campaign','departure','sequence','iata'];

    public $fillable = ['campaign','departure','sequence','iata'];
}


<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationRejectionLog extends Model
{

protected $connection = 'mysql2';
protected $table = 'inspire-flights_application_rejection_log';
//Naming this function the same confuses 
public function getapplication()
{
    return $this->belongsTo('App\Application','code','code');
}
//will not add created_at, update_at because they are not fields in our tables
public $timestamps = false;

}
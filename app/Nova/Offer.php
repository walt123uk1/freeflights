<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\BelongsTo;
use Twalton\OfferFlight\OfferFlight;
use Twalton\OfferPassenger\OfferPassenger;

class Offer extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Offer';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'offer';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'offer','type','code','created','currency','status'
    ];

    /**
    * The logical group associated with the resource.
    *
    * @var string
    */
    public static $group = 'Offers';


    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Text::make('Offer')->sortable()->hideWhenUpdating(),
            Date::make('Created')->format('DD/MM/YYYY')->sortable(),
            Text::make('Type')->sortable(),
            Text::make('Currency')->sortable(),
            Text::make('Status')->sortable(),
            OfferFlight::make(),
            OfferPassenger::make(),
            BelongsTo::make('Application','getapplication','App\Nova\Application')->searchable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}

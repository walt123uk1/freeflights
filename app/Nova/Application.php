<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\HasOne;
use Twalton\ApplicationDates\ApplicationDates;
use Twalton\ApplicationDestination\ApplicationDestination;
use Twalton\ApplicationPassenger\ApplicationPassenger;
use Twalton\ApplicationStageLog\ApplicationStageLog;

class Application extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Application';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'Application';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'code',
    ];
    /**
    * The logical group associated with the resource.
    *
    * @var string
    */
    public static $group = 'Application';

    public static function authorizable() { return true; }
    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [     
            Text::make('Code')->sortable(),
            Date::make('Date Due')->format('DD/MM/YYYY')->sortable(),
            Text::make('Language')->sortable(),
            HasOne::make('Application Code','getapplicationcode','App\Nova\ApplicationCode'),
            ApplicationDates::make(),
            ApplicationDestination::make(),
            ApplicationPassenger::make(),
            HasMany::make('Application Rejection Logs','getapplicationrejectionlog','App\Nova\ApplicationRejectionLog'),
            ApplicationStageLog::make(),
            //HasMany::make('Offers','getoffer','App\Nova\Offer'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    
    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}

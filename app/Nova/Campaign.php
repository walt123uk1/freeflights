<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\HasMany;
use Twalton\CampaignLanguage\CampaignLanguage;
use Twalton\CampaignFlightMatrix\CampaignFlightMatrix;
use Twalton\CampaignDepartureAirport\CampaignDepartureAirport;
use Twalton\CampaignDestinationAirport\CampaignDestinationAirport;
use Twalton\CampaignCheck\CampaignCheck;

class Campaign extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Campaign';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'Name','Title','Journey'
    ];

    /**
    * The logical group associated with the resource.
    *
    * @var string
    */
    public static $group = 'Campaign';

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            // Text::make('','campaign')->withMeta(['type' => 'hidden', 'style' => 'height:100px;'])->onlyOnForms(),
            Text::make('Name')->sortable(),
            Text::make('Prefix')->sortable(),
            Text::make('Title')->sortable(),
            Text::make('Journey')->sortable(),
            Date::make('Start')->format('DD/MM/YYYY')->sortable(),
            Date::make('End')->format('DD/MM/YYYY')->sortable(),
            CampaignLanguage::make(),
            HasMany::make('Campaign Urls','campaignurls','App\Nova\CampaignUrl'),
            HasMany::make('Application Code Batches','applicationcodebatches','App\Nova\ApplicationCodeBatch'),
            CampaignCheck::make(),
            CampaignFlightMatrix::make(),
            CampaignDepartureAirport::make(),
            CampaignDestinationAirport::make(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}

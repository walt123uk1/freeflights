<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Application extends Model
{
    protected $connection = 'mysql2';
     //need to set table name otherwise assumed to be applicatiomn 
    protected $table = 'inspire-flights_applications'; 
    //need to set otherwise it is assumed that primary key is id
    protected $primaryKey = 'code';
    public $incrementing = false;
    protected $keyType = 'string';

    protected $dates = ['date_due'];
    //will not add created_at, update_at because they are not fields in our tables
    public $timestamps = false;
    public function getapplicationcode()
    {
        return $this->hasOne('App\ApplicationCode','code','code');
    }
    public function getoffer()
    {
        return $this->hasMany('App\Offer','code','code');
    }
    
    public function getapplicationrejectionlog()
    {
        return $this->hasMany('App\ApplicationRejectionLog','code','code');
    }
}

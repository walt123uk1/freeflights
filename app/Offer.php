<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon; 
use Illuminate\Support\Facades\Log;


class Offer extends Model
{
    protected $connection = 'mysql2';
    //need to set table name otherwise assumed to be campaign 
    protected $table = 'inspire-flights_offers'; 
    //need to set otherwise it is assumed that primary key is id
    protected $primaryKey = 'offer';
    public $incrementing = false;
    protected $keyType = 'string';

    protected $dates = ['created'];
    //will not add created_at, update_at because they are not fields in our tables
    public $timestamps = false;
    public function getapplication()
    {
        return $this->belongsTo('App\Application', 'code', 'code');
    }
    public function setCreatedAttribute($value) {
        $this->attributes['created'] = Carbon::createFromFormat('Y-m-d', $value)->timestamp;
    }
}

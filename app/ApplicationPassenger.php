<?php

namespace App;
use Carbon\Carbon; 
use Log;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class ApplicationPassenger extends Model
{
    protected $connection = 'mysql2';
    //need to set table name otherwise assumed to be campaign 
    protected $table = 'inspire-flights_application_passengers'; 
    //need to set otherwise it is assumed that primary key is id
    protected $primaryKey = ['code','sequence'];
    public $incrementing = false;
    protected $keyType = 'string';

    protected $dates = ['dob'];
    //will not add created_at, update_at because they are not fields in our tables
    public $timestamps = false;
    public function getapplication()
    {
        return $this->belongsTo('App\Application', 'code', 'code');
    }
    public function setDobAttribute($value) {
        $this->attributes['dob'] = Carbon::createFromFormat('Y-m-d', $value)->timestamp;
    }   
    // public function getDateAttribute($value) {
    //     return Carbon::createFromFormat('U', $value)->format('d/m/Y');
    // }

    public function getFirstNameAttribute($value) {
        //returns objects in an array hence $plain_text[0]->plain_text 
        $plain_text = DB::select('SELECT AES_DECRYPT("'.$value.'", "vYURNY2Kh2KmvtxaGrar") AS plain_text');
        return $plain_text[0]->plain_text;
    } 
    public function getLastNameAttribute($value) {
        //returns objects in an array hence $plain_text[0]->plain_text 
        $plain_text = DB::select('SELECT AES_DECRYPT("'.$value.'", "W6wcBUwhqAJe3AGdRsQW") AS plain_text');
        return $plain_text[0]->plain_text;
    } 
    public function getPassportAttribute($value) {        
        //returns objects in an array hence $plain_text[0]->plain_text 
        // $plain_text = DB::select('SELECT AES_DECRYPT("'.$value.'", "86mHFkwsAVKkqne84iMs") AS plain_text');
        // return $plain_text[0]->plain_text;
    } 
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB; 
use App\Campaignurl;
use App\Campaign;
use App\CampaignWebsite;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (url()->current()=='http://freeflights.test'){
            return redirect('nova');
        }else{             
            $pieces = explode("/", url()->current());
            $campaign_url = Campaignurl::find($pieces[2]); 
            if ($campaign_url=='') {    
                return redirect('http://inspirewebsite.co.uk/');
            }else{    
                $campaign_langs = Campaign::where( 'inspire-flights_campaigns.campaign', '=', $campaign_url->campaign)
                ->join('inspire-flights_campaign_languages','inspire-flights_campaigns.campaign','=','inspire-flights_campaign_languages.campaign')
                ->get();
                foreach($campaign_langs as $campaign_lang) {
                    $mode = '§'.$campaign_lang->language.':'.$campaign_lang->journey.':'.$campaign_lang->prefix;   
                    //USE MODE EG EN:2-UPL:RBG TO FIND PAGE
                    $copys = CampaignWebsite::where( 'mode', '=', $mode)
                    ->get();
                    //Add language field to copy record so can easily differentiate
                    $copys->add(['language' => $campaign_lang->language]);
                    //Add language array for display
                }
                return view('campaign.website', compact('mode'));
            }
        }
    }

    /**
     * Return to page about you.
     *
     * @return \Illuminate\Http\Response
     */
    public function aboutyou()
    {
        return view('campaign.aboutyou');
    } 
    /**
    * Return to page about you.
    *
    * @return \Illuminate\Http\Response
    */
   public function aboutyoupost()
   {
       return view('campaign.aboutyou');
   }

   /**
    * Return to page yourflight.
    *
    * @return \Illuminate\Http\Response
    */
   public function yourflight()
   {
       return view('campaign.yourflight');
   }

   /**
    * Return to page yourflight.
    *
    * @return \Illuminate\Http\Response
    */
   public function yourflightpost()
   {
       return view('campaign.yourflight');
   }

   /**
    * Return to page review.
    *
    * @return \Illuminate\Http\Response
    */
   public function review()
   {
       return view('campaign.review');
   }

   /**
    * Return to page review.
    *
    * @return \Illuminate\Http\Response
    */
   public function reviewpost()
   {
       return view('campaign.review');
   }

   /**
    * Return to page thanks.
    *
    * @return \Illuminate\Http\Response
    */
   public function thanks()
   {
       return view('campaign.thanks');
   }

   /**
    * Return to page thanks.
    *
    * @return \Illuminate\Http\Response
    */
   public function thankspost()
   {
       return view('campaign.thanks');
   }

    /**
     * Return to page FAQ.
     *
     * @return \Illuminate\Http\Response
     */
    public function faqs()
    {
        return view('campaign.faq');
    }
    /**
     * Return to page tandc.
     *
     * @return \Illuminate\Http\Response
     */
    public function tandc()
    {
        return view('campaign.tandc');
    }
    /**
     * Return to page contact.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
        return view('campaign.contact');
    }
    /**
     * Return to page about.
     *
     * @return \Illuminate\Http\Response
     */
    public function aboutus()
    {
        return view('campaign.aboutus');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

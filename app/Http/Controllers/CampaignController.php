<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon; 

use App\Campaign;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $campaigns = Campaign::sortable(['campaign'=> 'desc'])->paginate(10);
        return view('campaign.campaign', ['campaigns' => $campaigns]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //validation
        $data = request()->validate([
            'name' => 'required|max:50',
            'prefix' => 'bail|required|unique:inspire-flights_campaigns|max:3',
            'journey' => 'required|max:10',
            'startdate' => 'required|date_format:d/m/Y',
            'enddate' => 'required|date_format:d/m/Y',
        ]);
        //assign each value according to existing code and database
        $campaign_no = Campaign::max('campaign') + 1;
        $favicon = "/inspire_flights/sites/".$data['prefix']."/images/favicon.ico";
        $css = "/inspire_flights/sites/".$data['prefix']."/css/".$data['prefix'].".css";
        //convert date to timestamp
        $startdate = Carbon::createFromFormat('d/m/Y', $data['startdate'])->timestamp;
        $enddate = Carbon::createFromFormat('d/m/Y', $data['enddate'])->timestamp;
        
        //create new record
        $campaign = new Campaign;
        $campaign->campaign = $campaign_no;
        $campaign->name = $data['name'];
        $campaign->prefix = $data['prefix'];
        $campaign->journey = $data['journey'];
        $campaign->dolphin_id = strtoupper($data['prefix']);
        $campaign->status = 0;
        $campaign->stealth_prefix = 1;
        $campaign->generated_batch = 0;
        $campaign->share_url = 0;
        $campaign->use_code = 1;
        $campaign->currency = 'EUR';
        $campaign->title = $data['name'];
        $campaign->responsive = 1;
        $campaign->highlight = '#E83759';
        $campaign->contact_name = 'Inspire Flights';
        $campaign->contact_address = 'customerservice@inspireemail.co.uk';
        $campaign->formality = 2;
        $campaign->marketing = 0;
        $campaign->has_poll = 0;
        $campaign->upload_poll = '';
        $campaign->personal_details_stage = 1;
        $campaign->mypassport_stage = 2;
        $campaign->upload_stage = 0;
        $campaign->receipt_upload_stage = 0;
        $campaign->flights_details_stage = 1;
        $campaign->passenger_stage = 2;
        $campaign->address_stage = 2;
        $campaign->address_type = 1;
        $campaign->google_analytics = '';
        $campaign->favicon = $favicon;
        $campaign->logo = '';
        $campaign->css = $css;
        $campaign->rix = 0;
        $campaign->terms_matrix = 0;
        $campaign->start = $startdate;
        $campaign->end = $enddate;
        $campaign->fly_from = 30;
        $campaign->fly_until = $enddate;
        $campaign->stay_length = 28;
        $campaign->return_within = 1;
        $campaign->mail_in_due = 7;
        $campaign->details_due = 7;
        $campaign->upload_due = 0;
        $campaign->offer_due = 14;
        $campaign->offer_accept_due = 2;
        $campaign->email_pdf = 0;
        $campaign->mail_in_receipt = 0;
        $campaign->contact_usedcode = 0;
        $campaign->passport_upload = 0;
        $campaign->xmas = 0;
        $campaign->airlines = 1;
        $campaign->self_serve = 0;
        $campaign->self_search_count = 0;
        $campaign->self_serve_per_day = 0;
        $campaign->extension = '';
        $campaign->ext_version ='';
        //save new record
        $campaign->save();

        // return to page with date reloaded
        $campaigns = Campaign::sortable(['campaign'=> 'desc'])->paginate(10);
        return view('campaign.campaign', ['campaigns' => $campaigns]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $campaign
     * @return \Illuminate\Http\Response
     */
    public function destroy($campaign)
    {     
        // delete single campaign primary key - campaign
        $campaign_del = Campaign::find($campaign);
        $campaign_del->delete();
        // return to page with date reloaded
        $campaigns = Campaign::sortable(['campaign'=> 'desc'])->paginate(10);
        return view('campaign.campaign', ['campaigns' => $campaigns]);
    }
}

<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon; 

class Campaign extends Model
{
    protected $connection = 'mysql2';
    //need to set table name otherwise assumed to be campaign 
    protected $table = 'inspire-flights_campaigns'; 
    //need to set otherwise it is assumed that primary key is id
    protected $primaryKey = 'campaign';

    //will not add created_at, update_at because they are not fileds in our tables
    public $timestamps = false;
    public $autoincrement = false;

    protected $dates = ['start', 'end', 'fly_until'];
    //allows tables to be sortable (alongside pagination)
    use Sortable;
    public $sortable = ['name','journey','start', 'end','title','prefix'];

    public $fillable = ['campaign','name','start', 'end','journey','title','prefix'];
    
    protected $attributes = [
        'campaign' => 0,
        'dolphin_id' => '',
        'status' => 0,
        'stealth_prefix' => 1,
        'generated_batch' => 0,
        'share_url' => 0,
        'use_code' => 1,
        'currency' => 'EUR',
        'responsive' => 1,
        'highlight' => '#E83759',
        'contact_name' => 'Inspire Flights',
        'contact_address' => 'customerservice@inspireemail.co.uk',
        'formality' => 2,
        'marketing' => 0,
        'has_poll' => 0,
        'upload_poll' => '',
        'personal_details_stage' => 1,
        'mypassport_stage' => 2,
        'upload_stage' => 0,
        'receipt_upload_stage' => 0,
        'flights_details_stage' => 1,
        'passenger_stage' => 2,
        'address_stage' => 2,  
        'address_type' => 1,
        'google_analytics' => '',
        'logo' => '',
        'rix' => 0,
        'terms_matrix' => 0,
        'fly_from' => 30,
        'stay_length' => 28,
        'return_within' => 1,
        'mail_in_due' => 7,
        'details_due' => 7,
        'upload_due' => 0,
        'offer_due' => 14,
        'offer_accept_due' => 2,
        'email_pdf' => 0,
        'mail_in_receipt' => 0,
        'contact_usedcode' => 0,
        'passport_upload' => 0,
        'xmas' => 0,
        'airlines' => 1,
        'self_serve' => 0,
        'self_search_count' => 0,
        'self_serve_per_day' => 0,
        'extension' => '',
        'ext_version' => '',
    ];

    public function campaignurls()
    {
        return $this->hasMany('App\CampaignUrl','campaign','campaign');
    }
    public function campaignlangs()
    {
        return $this->hasMany('App\CampaignLang','campaign','campaign');
    }
    public function applicationcodes()
    {
        return $this->hasMany('App\ApplicationCode','campaign','campaign');
    }
    public function applicationcodebatches()
    {
        return $this->hasMany('App\ApplicationCodeBatch','campaign','campaign');
    }
    public static function boot()
    {
        parent::boot();
        static::creating(function($model)
        {
            $model->campaign = Campaign::max('campaign') + 1;
        });
    }
    // public function setCampaignAttribute($value) {
    //     $this->attributes['campaign'] = Campaign::max('campaign') + 1;
    // }
    public function getCampaignAttribute($origvalue) {
        if ($origvalue==0) {
            $value = Campaign::max('campaign');
        }else{
            $value = $origvalue;
        }
        return $value;
    }
    public function setStartAttribute($value) {
        $this->attributes['start'] = Carbon::createFromFormat('Y-m-d', $value)->timestamp;
    }
    public function setEndAttribute($value) {
        $this->attributes['end'] = Carbon::createFromFormat('Y-m-d', $value)->timestamp;
        $this->attributes['fly_until'] = Carbon::createFromFormat('Y-m-d', $value)->timestamp;
    }
    public function setPrefixAttribute($value) {
        $css = "/inspire_flights/sites/".$value."/css/".$value.".css";
        $this->attributes['css'] = $css;
        $favicon = "/inspire_flights/sites/".$value."/images/favicon.ico";
        $this->attributes['favicon'] = $favicon;
        $this->attributes['dolphin_id'] = strtoupper($value);
        $this->attributes['prefix'] = $value;
    }
}

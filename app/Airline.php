<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airline extends Model
{

/**
* @var  string
*/
protected $connection = 'mysql2';
//need to set table name otherwise assumed to be campaignUrls 
protected $table = 'inspire-flights_airlines'; 
//need to set otherwise it is assumed that primary key is id
protected $primaryKey = 'iata';
public $incrementing = false;
protected $keyType = 'string';

//will not add created_at, update_at because they are not fields in our tables
public $timestamps = false;

protected $casts = [
];

}
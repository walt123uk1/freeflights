<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;

class ApplicationDate extends Model
{    
    protected $connection = 'mysql2';
    //need to set table name otherwise assumed to be campaignLang 
    protected $table = 'inspire-flights_application_dates';
    //need to set otherwise it is assumed that primary key is id
    protected $primaryKey = ['code','sequence'];
    public $incrementing = false;
    //protected $dates = ['date'];
    //Naming this function the same confuses 
    public function application()
    {
        return $this->hasOne('App\Application','code','code');
    }
    //will not add created_at, update_at because they are not fields in our tables
    public $timestamps = false;
    //allows tables to be sortable (alongside pagination)
    use Sortable;
    public $sortable = ['code','sequence','date','stay'];

    public $fillable = ['code','sequence','date','stay'];
     
    // public function setDateAttribute($value) {
    //     $this->attributes['date'] = Carbon::createFromFormat('Y-m-d', $value)->timestamp;
    // }    
    // public function getDateAttribute($value) {
    //     return Carbon::createFromFormat('U', $value)->format('d/m/Y');
    // }
}

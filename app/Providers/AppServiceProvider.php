<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Debugbar;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //the following line added by Daniel due to experiencing this issue on staging deploy: https://laravel-news.com/laravel-5-4-key-too-long-error/
        //see also https://laravel.com/docs/master/migrations#creating-indexes
        Schema::defaultStringLength(191);
        // DB::listen(function($sql) {
        //     Log::info($sql->sql);
        //     Log::info($sql->bindings);
        //     Log::info($sql->time);
        // });
    }
}

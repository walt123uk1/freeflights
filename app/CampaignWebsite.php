<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignWebsite extends Model
{
    protected $connection = 'mysql2';
    //need to set table name otherwise assumed to be campaignUrls 
    protected $table = 'inspire-flights_system_copy'; 
    //need to set otherwise it is assumed that primary key is id
    protected $primaryKey = null;
    public $incrementing = false;

    //will not add created_at, update_at because they are not fields in our tables
    public $timestamps = false;
    //allows tables to be sortable (alongside pagination)
}

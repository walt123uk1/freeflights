<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationCodeBatch extends Model
{

/**
* @var  string
*/
protected $connection = 'mysql2';
//need to set table name otherwise assumed to be campaignUrls 
protected $table = 'inspire-flights_application_code_batches'; 
//need to set otherwise it is assumed that primary key is id
protected $primaryKey = 'id';

//will not add created_at, update_at because they are not fields in our tables
public $timestamps = false;    
public function getcampaign()
{
    return $this->belongsTo('App\Campaign', 'campaign', 'campaign');
}
protected $casts = [
];

}
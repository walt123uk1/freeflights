<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class ApplicationDestination extends Model
{    
    protected $connection = 'mysql2';
    //need to set table name otherwise assumed to be campaignLang 
    protected $table = 'inspire-flights_application_destinations';
    //need to set otherwise it is assumed that primary key is id
    protected $primaryKey = ['code','sequence'];
    public $incrementing = false;
    //Naming this function the same confuses 
    public function application()
    {
        return $this->hasOne('App\Application','code','code');
    }
    //will not add created_at, update_at because they are not fields in our tables
    public $timestamps = false;
    //allows tables to be sortable (alongside pagination)
    use Sortable;
    public $sortable = ['code','sequence','destination'];

    public $fillable = ['code','sequence','destination'];

}

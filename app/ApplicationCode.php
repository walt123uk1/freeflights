<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon; 

class ApplicationCode extends Model
{    
    protected $connection = 'mysql2';
    //need to set table name otherwise assumed to be campaign 
    protected $table = 'inspire-flights_application_codes'; 
    //need to set otherwise it is assumed that primary key is id
    protected $primaryKey = 'code';
    public $incrementing = false;
    protected $keyType = 'string';

    protected $dates = ['expires'];
    public $fillable = ['code'];
    //will not add created_at, update_at because they are not fields in our tables
    public $timestamps = false;
    public function getcampaign()
    {
        return $this->belongsTo('App\Campaign', 'campaign', 'campaign');
    }
    public function application()
    {
        return $this->hasOne('App\Application','code','code');
    }
    public function setExpiresAttribute($value) {
        $this->attributes['expires'] = Carbon::createFromFormat('Y-m-d', $value)->timestamp;
    }
}
@extends('layouts.app') 
@section('content')			
	<p>About inspire</p>
	<p><strong>Company No:</strong><br /> 05609301</p>
	<p><strong>Register Address:</strong><br />Inspire Europe Ltd<br />Victoria House,<br /> 19 to 21 Ack Lane East, <br /> Bramhall,<br />Cheshire SK7 2BE<br />Great Britain</p>
	<p>Tel.: 0044 (0) 1614406600<br /> Fax: 0044 (0) 1616010231</p>
	<p><strong>Contact Address:</strong><br />Inspire Europe Ltd<br />Marsstr. 4<br />80335 München</p>
	<p>E-mail: <a href="mailto:info@inspireemail.co.uk">info@inspireemail.co.uk</a><br /> <a href="http://www.inspirewebsite.co.uk/" target="_blank">www.inspirewebsite.co.uk</a></p>
	<h2></h2>
	<p>&copy;inspire, Bramhall, England 2012. All rights reserved.</p>
@endsection

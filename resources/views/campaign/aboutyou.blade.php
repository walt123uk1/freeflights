@extends('layouts.app') 
@section('content')
	<h2>Your&nbsp;application</h2>
	<p>Please complete the application form with your personal details.</p>
	<p>Important: Please enter your full names and surname as they are shown in your passport.
		<br><br>We only use your details to process your free flight application. We never pass your details on to others.
		<br><br>
	</p>
	<form id="form-details" method="post" action="/yourflight" enctype="multipart/form-data" accept-charset="UTF-8" class="responsive">
		{{ csrf_field() }}	
		<input type="hidden" name="form-name" value="form-details">
		<input name="form-details_form-submitted" id="form-details_form-submitted" type="hidden">
		<div class="button-row row">
			<div class="my-1 col-sm-3  offset-lg-2 col-lg-2">
				<a class="btn btn-primary btn-block" role="button">About You</a>
			</div><!-- /col -->
			<div class="my-1 col-sm-3 col-lg-2">
				<a class="btn btn-secondary btn-block" role="button">Your Flight</a>
			</div><!-- /col -->
			<div class="my-1 col-sm-3 col-lg-2">
				<a class="btn btn-secondary btn-block" role="button">Confirmation</a>
			</div><!-- /col -->
			<div class="my-1 col-sm-3 col-lg-2">
				<a class="btn btn-secondary btn-block" role="button">Print&nbsp;PDF</a>
			</div><!-- /col -->
		</div><!-- /row -->
			<div class="col-sm-12">
				<p>Required&nbsp;fields are marked with a <span class="required">*</span></p>	
			</div>		
			<div class="form-group row">
				<label for="name" class="offset-sm-2 col-sm-2 col-form-label">*First Name</label>
				<div class="col-sm-6">
				<input type="text" class="form-control" id="name" placeholder="First Name">
				</div>
			</div>
			<div class="form-group row">
				<label for="surname" class="offset-sm-2 col-sm-2 col-form-label">*Surname</label>
				<div class="col-sm-6">
				<input type="text" class="form-control" id="surname" placeholder="Surname">
				</div>
			</div>
			<div class="form-group row">
				<label for="staticEmail" class="offset-sm-2 col-sm-2 col-form-label">*Email</label>
				<div class="col-sm-6">
					<input type="email" class="form-control" id="staticEmail" placeholder="email@example.com">
				</div>
				<div class="offset-sm-4 col-sm-6">
					<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
				</div>
			</div>
			<div class="form-check">
				<div class="offset-sm-2 col-sm-8">
					<input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
					<label class="form-check-label" for="defaultCheck1">
						*Yes, I would like to apply for my free flight and accept the 
						<a href="/tandc">terms and conditions</a> of this campaign.
					</label>
				</div>
			</div>
			<br>	
		<button class="btn btn-danger btn-lg my-3" name="form-code-login_form-submitted" value="go">
			Submit	
		</button>
	</form>
@endsection

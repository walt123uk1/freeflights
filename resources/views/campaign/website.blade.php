@extends('layouts.app') 
@section('content')
<div class="content">
	<h2>Take off with Inspire</h2>
	<p>With the purchase of a participating product(s) (please see FAQ's or T&amp;C's for participating models)
		from a participating store during the promotional period,you will receive a free flight!
		<br><br>
		Apply you here from [application start date].
		<br><br>
		Pack your bags and go!
		<br><br>
		Please have a look at our FAQ’s for more information.
		<br><br>
	</p>
	<form id="form-code-login" action="/aboutyou" method="post" enctype="multipart/form-data" accept-charset="UTF-8" class="responsive">
		{{ csrf_field() }}	
		<input type="hidden" name="form-name" value="form-code-login">
		<input name="form-code-login_form-submitted" id="form-code-login_form-submitted" type="hidden">
		<h2>Enter your voucher&nbsp;code here:</h2>
		<input name="form-code-login_code" id="form-code-login_code" class="form-control form-control-lg" type="text" placeholder="">
		<br>
		<label id="form-code-login_code_report" class="report">
		</label>	
		<button class="btn btn-danger btn-lg" name="form-code-login_form-submitted" value="go">
			Submit	
		</button>
	</form>
</div>
@endsection

@extends('layouts.app') 
@section('content')
	<h3>HOW DO I CLAIM MY FREE FLIGHT?</h3>
	<p>
		<strong>Step 1</strong>
	</p>
	<p>Purchase a participating product in one of the participating retailers (Technopark, PBT, Cholodilnik.ru, Evropa) during the promotional period from September 10, until October 15, 2019. 
		<br />Keep the sales receipt of your purchase.
	</p>
	<p>
		<strong>Step 2</strong>
	</p>
	<p>To claim your free flight please register on www.flywithcandy.ru with the Promo Code received upon purchase, enter your personal data as well as 3 travel dates and destinations. Please choose your departure airport, three destinations you would like to fly to and three possible travel dates. Please note that no destinations and no travel dates will be treated as a priority over another.</p>
	<p>
		<strong>Step 3</strong>
	</p>
	<p>Print off your application PDF and send it together with the sales receipt original by ordinary mail to:</p>
	<p>
		<strong>Inspire-Rix</strong>
		<br />
		<strong>Fly with Candy</strong>
		<br />Regus Sinop Center, 4th floor,
		<br />Sinopskaya naberezhnaya, 22,
		<br />Saint Petersburg, 191167 Russia
		<br />
		<br />Your application must be completed and posted within the redemption window between October 21 and October 27, 2019. Claims posted after this term shall be excluded from the promotion.
	</p>
	<p>
		<strong>Step 4</strong>
	</p>
	<p>After successful validation of your application you will receive a confirmation web-link via email. You will have to complete your application using the confirmation link within 7 calendar days after receipt of the link, i.e. provide your personal data, details of companion passengers, upload scan of the main page of your and companion passengers’ travel passports and send this data online.   </p>
	<p>
		<strong>Step 5</strong>
	</p>
	<p>Within 14 days of completion of your application, you will receive a flight offer via email. In order to benefit from this offer it will have to be accepted within 48 hours.</p>
	<p>If there are any additional passengers or costs for additional services your flight will be booked after payment for these has been taken.</p>
	<h3>WHICH CANDY PRODUCTS ARE PARTICIPATING?</h3>
	<p>The following Candy models are participating in the promotion:</p>
	<div class="container-fluid">
		<div id="product-table" class="row justify-content-center">
			<div class="col-sm-8">
				<table class="table table-striped">	
					<tbody>
						<tr><td>Candy FCT615XL</td></tr>
						<tr><td>Candy FCT615NXL</td></tr>
						<tr><td>Candy FCT615WXL</td></tr>
						<tr><td>Candy FCP625XL/E</td></tr>
						<tr><td>Candy FCP625NXL/E</td></tr>
						<tr><td>Candy FCP625WXL/</td></tr>
						<tr><td>Candy FCP605XL/E1</td></tr>
						<tr><td>Candy FCP605NXL/E</td></tr>
						<tr><td>Candy FCP605WXL/E</td></tr>
						<tr><td>Candy FCP612X/E</td></tr>
						<tr><td>Candy FCP612N/E</td></tr>
						<tr><td>Candy FCP612W/E</td></tr>
						<tr><td>Candy FCC614GH/E</td></tr>
						<tr><td>Candy FCC614BA/E</td></tr>
						<tr><td>Candy FCC614BA/E</td></tr>
						<tr><td>Candy CLGC64SGBA</td></tr>
						<tr><td>Candy CLGC64SGGH</td></tr>
						<tr><td>Candy CHG6D4WGTWA</td></tr>
						<tr><td>Candy CHG6D4WGTGH</td></tr>
						<tr><td>Candy CHG6BF4WGTWA</td></tr>
						<tr><td>Candy CHG6BF4WGTGH</td></tr>
						<tr><td>Candy CHG6DWPX</td></tr>
						<tr><td>Candy CHG6BF4WPX</td></tr>
						<tr><td>Candy CSG64SWGX</td></tr>
						<tr><td>Candy CSG64SWGN</td></tr>
						<tr><td>Candy CSG64SWGB</td></tr>
						<tr><td>Candy CVG64SGX</td></tr>
						<tr><td>Candy CVG64STGN</td></tr>
						<tr><td>Candy CVG64STGB</td></tr>
						<tr><td>Candy CI640CB</td></tr>
						<tr><td>Candy CH64FC</td></tr>
						<tr><td>Candy CH64DXT</td></tr>
						<tr><td>Candy CH64CCB</td></tr>
						<tr><td>Candy CH64BFT</td></tr>
						<tr><td>Candy CH64CCW</td></tr>
						<tr><td>Candy CH64BVT</td></tr>
						<tr><td>Candy CH64DCT</td></tr>
						<tr><td>Candy CSS4 1282 D1/2-07</td></tr>
						<tr><td>Candy GVS4 137 TWC1/2-07</td></tr>
						<tr><td>Candy CSWS40 364 D/2-07</td></tr>
						<tr><td>Candy GVSW40 364 TWHC-07</td></tr>
						<tr><td>Candy GVF4 137 LWHC/2-07</td></tr>
						<tr><td>Candy CSS44 1282DB/2-07</td></tr>
						<tr><td>Candy SGV44 128 TWB3-07</td></tr>
						<tr><td>Candy GVF44 138 LWHC3-07</td></tr>
						<tr><td>Candy GVS44 138 TWHC-07</td></tr>
						<tr><td>Candy GVS44 128 TWC3-07</td></tr>
						<tr><td>Candy CS C9LG-07</td></tr>
						<tr><td>Candy CS4 H7A1DE-07/</td></tr>
						<tr><td>Candy CS C10DBGX-07</td></tr>
						<tr><td>Candy GSV C10DSGX-07</td></tr>
						<tr><td>Candy GVS 4137 TWC 1/2-07</td></tr>
						<tr><td>Candy SGV 44128 TWB 3-07</td></tr>
						<tr><td>Candy GVS 34116 TC 2/2-07</td></tr>
					</tbody>
				</table>
			</div>	
		</div>	
	</div>		
	<h3>
		<br />WHICH DESTINATIONS CAN I TRAVEL TO?
	</h3>
	<p>Please tell us three different destinations you would like to travel to and we will then check our availability. Please note that not all destinations are available from a specific departure airport.</p>
	<p>Possible departure airports and destinations are the following:</p>
	<div class="container-fluid">
		<div id="flight-table" class="row justify-content-center">
			<div class="col-sm-4 col-lg-2">
				<table class="table table-striped">
					<thead class="thead-dark">
						<tr><th>Moscow</th></tr>
					</thead>	
					<tbody>
						<tr><td>Amsterdam</td></tr>
						<tr><td>Barcelona</td></tr>
						<tr><td>Berlin</td></tr>
						<tr><td>Brussels</td></tr>
						<tr><td>Budapest</td></tr>
						<tr><td>Copenhagen</td></tr>
						<tr><td>Frankfurt</td></tr>
						<tr><td>Helsinki</td></tr>
						<tr><td>Istanbul</td></tr>
						<tr><td>London</td></tr>
						<tr><td>Minsk</td></tr>
						<tr><td>Milan</td></tr>
						<tr><td>Paris</td></tr>
						<tr><td>Prague</td></tr>
						<tr><td>Riga</td></tr>
						<tr><td>Rome</td></tr>
						<tr><td>Adler - Sochi</td></tr>
						<tr><td>Simferopol</td></tr>
						<tr><td>Stockholm</td></tr>
						<tr><td>Warsaw</td></tr>
						<tr><td>Vienna</td></tr>
						<tr><td>Zurich</td></tr>
					</tbody>
				</table>
			</div>		
			<div class="col-sm-4 col-lg-2">
				<table class="table table-striped">
					<thead class="thead-dark">
						<tr><th>Saint-Petersburg</th></tr>
					</thead>
					<tbody>
						<tr><td>Amsterdam</td></tr>
						<tr><td>Barcelona</td></tr>
						<tr><td>Berlin</td></tr>
						<tr><td>Brussels</td></tr>
						<tr><td>Budapest</td></tr>
						<tr><td>Copenhagen</td></tr>
						<tr><td>Frankfurt</td></tr>
						<tr><td>Helsinki</td></tr>
						<tr><td>Istanbul</td></tr>
						<tr><td>London</td></tr>
						<tr><td>Minsk</td></tr>
						<tr><td></td></tr>
						<tr><td>Paris</td></tr>
						<tr><td>Prague</td></tr>
						<tr><td>Riga</td></tr>
						<tr><td>Rome</td></tr>
						<tr><td>Adler - Sochi</td></tr>
						<tr><td>Simferopol</td></tr>
						<tr><td>Stockholm</td></tr>
						<tr><td>Warsaw</td></tr>
						<tr><td>Vienna</td></tr>
						<tr><td>Zurich</td></tr>
					</tbody>
				</table>
			</div>	
			<div class="col-sm-4 col-lg-2">
				<table class="table table-striped">
					<thead class="thead-dark">
					<tr><th>Minsk</th></tr>
					</thead>
					<tbody>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td>Berlin</td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td>London</td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td>Paris</td></tr>
						<tr><td>Prague</td></tr>
						<tr><td>Riga</td></tr>
						<tr><td></td></tr>
						<tr><td>Adler - Sochi</td></tr>
						<tr><td>Simferopol</td></tr>
						<tr><td></td></tr>
						<tr><td>Warsaw</td></tr>
						<tr><td></td></tr>
						<tr><td>Zurich</td></tr>
					</tbody>
				</table>			
			</div>
			<div class="col-sm-4 col-lg-2">
				<table class="table table-striped">
					<thead class="thead-dark">
						<tr><th>Bramhall</th></tr>
					</thead>
					<tbody>
						<tr><td>Amsterdam</td></tr>
						<tr><td></td></tr>
						<tr><td>Berlin</td></tr>
						<tr><td>Brussels</td></tr>
						<tr><td></td></tr>
						<tr><td>Copenhagen</td></tr>
						<tr><td>Frankfurt</td></tr>
						<tr><td>Helsinki</td></tr>
						<tr><td></td></tr>
						<tr><td>London</td></tr>
						<tr><td>Minsk</td></tr>
						<tr><td></td></tr>
						<tr><td>Paris</td></tr>
						<tr><td>Prague</td></tr>
						<tr><td>Riga</td></tr>
						<tr><td>Rome</td></tr>
						<tr><td>Adler - Sochi</td></tr>
						<tr><td></td></tr>
						<tr><td>Stockholm</td></tr>
						<tr><td>Warsaw</td></tr>
						<tr><td></td></tr>
						<tr><td>Zurich</td></tr>
					</tbody>
				</table>	
			</div>	
			<div class="col-sm-4 col-lg-2">
				<table class="table table-striped">
					<thead class="thead-dark">
						<tr><th>Lisbon</th></tr>
					</thead>
					<tbody>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td>Berlin</td></tr>
						<tr><td>Brussels</td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td>London</td></tr>
						<tr><td>Minsk</td></tr>
						<tr><td></td></tr>
						<tr><td>Paris</td></tr>
						<tr><td>Prague</td></tr>
						<tr><td>Riga</td></tr>
						<tr><td>Rome</td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td>Warsaw</td></tr>
						<tr><td></td></tr>
						<tr><td>Zurich</td></tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<h3>CAN I BOOK A TICKET FOR MY PARTNER OR A FRIEND?</h3>
	<p>Yes. We are happy to add companion passengers to your booking. You can add up to three companion passengers to your application. Any additional passengers will be charged at the current flight price at the time of booking including any taxes and charges. Any additional charges by the airline, such as golf bags, additional luggage, priority boarding etc. will have to be paid for. Some airlines charge for all checked baggage.</p>
	<h3>WHEN CAN I TRAVEL?</h3>
	<p>All travel (departure and return) must be completed by 01.08.2020 and is dependent on the carrier’s available flight schedules. All flight applications have to be received at least 45 days before the first date of travel. The offer is not valid for journeys during peak periods and over Christmas Day and New Year’s Day. Some periods (e.g. religious and school holidays) and dates may be restricted depending on the chosen destination and travel time. The availability of flights may also be restricted by special events, trade fairs or local holidays.</p>
	<h3>WHAT IS THE MINIMUM AND MAXIMUM STAY AT MY DESTINATION?</h3>
	<p>The minimum stay is 2 calendar days.. The maximum stay is 30 calendar days.</p>
	<h3>CAN I GIVE MY FREE FLIGHT AWAY TO A FRIEND OR PARTNER?</h3>
	<p>No. Tickets are personal and non-transferable. Bookings cannot be requested or made by third parties and flights may not be booked by third parties using the name of the participant.</p>
	<h3>CAN I EXCHANGE MY FREE FLIGHT FOR THE VALUE OF THE FLIGHT IN CASH?</h3>
	<p>No cash alternative will be offered in exchange for the ticket or promotional offer.</p>
	<h3>WHAT DO I NEED TO BE AWARE OF WHEN CHOOSING MY THREE TRAVEL DATES?</h3>
	<p>When choosing your three travel dates, please ensure that your first chosen travel date must be at least 45 calendar days away from the date of application. Your travel dates have to be in different months and 30 days apart from one another.</p>
	<h3>MY FRIEND ALSO HAS A FREE FLIGHT VOUCHER, CAN WE FLY TOGETHER?</h3>
	<p>No. Applications will not be processed in conjunction with one another.</p>
	<h3>CAN I CHOSE MY FLIGHT TIMES OR AIRLINE?</h3>
	<p>No. The choice of carrier, flight times and route is at the sole discretion of The Organizer. The Participant is not entitled to demand a particular carrier, flight time or route.</p>
	<h3>HOW MUCH TIME DO I HAVE TO APPLY FOR MY FREE FLIGHT?</h3>
	<p>Your application must be completed and posted within the redemption window between October 21 and October 27, 2019. Claims posted after this term shall not participate and shall be excluded from the promotion.</p>
	<h3>DO I HAVE TO SEND IN THE PURCHASE RECEIPT?</h3>
	<p>Yes. The Organizer will not process any flight bookings for customers who do not mail their sales receipt attached to their application. </p>
	<h3>WILL THERE BE ANY ADDITIONAL COST?</h3>
	<p>Your free flight will be booked at no cost to you. Your carrier base fare, taxes, passenger duty, fuel surcharges, service charge and airport fee will be covered by your free flight. All other costs associated with the flight will be borne by the Participant and are excluded from the promotional offer. Such other costs include, but are not limited to, any baggage or other charges made in connection with the flight or in addition to the fare such as any insurance, any additional fees for card payments, any fees for hotel, car hire, airport transfers, taxis, car park, seat upgrade, food and drink, visas, immigration documents or any other additional services. The Participant must pay for all additional services, additional passengers and cancellations or alterations via a credit card in their name.</p>
	<h3>I HAVE RECEIVED MY OFFER, HOWEVER I WOULD LIKE TO REMOVE MY ADDITIONAL PASSENGER(S). IS THAT POSSIBLE?</h3>
	<p>Should you decide to travel without accompanying passengers, please still accept your offer. Once accepted, please use the contact form on the campaign website to let us know ASAP that you would like to travel without any additional passengers.</p>
	<h3>CAN I MAKE ANY CHANGES TO MY FLIGHT AFTER IT IS BOOKED?</h3>
	<p>No changes or cancellations of flights are possible after the booking has been made.</p>
	<h3>WHEN WILL I RECEIVE MY BOOKING CONFIRMATION / TICKET?</h3>
	<p>Once you have accepted your offered flight (and paid for any additional passengers or charges if applicable) we will book your flight and email you your e-ticket as soon as possible. Please be aware that bookings can’t always be made immediately after acceptance of an offer.</p>
	<h3>I HAVE FURTHER QUESTIONS. WHAT CAN I DO?</h3>
	<p>Please refer to our contact form on www.flywithcandy.ru. You can also contact us on 81080026015011 Monday – Friday, 10am - 5pm.(Moscow Time)</p>
@endsection

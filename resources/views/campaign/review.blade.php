@extends('layouts.app') 
@section('content')<h2>
Your Application	</h2>

<p>Please check that the information you have provided is correct. If you need to make any changes, please go back one step and edit as required.<br /><br />To confirm your application, press submit.</p>	
	<form id="form-code-login" action="/thanks" method="post" enctype="multipart/form-data" accept-charset="UTF-8" class="responsive">
	{{ csrf_field() }}	
		<input type="hidden" name="form-name" value="form-details">
		<input name="form-details_form-submitted" id="form-details_form-submitted" type="hidden">
		<div class="button-row row">
			<div class="my-1 col-sm-3  offset-lg-2 col-lg-2">
				<a class="btn btn-primary  btn-block" href="aboutyou" role="button">About You</a>
			</div><!-- /col -->
			<div class="my-1 col-sm-3 col-lg-2">
				<a class="btn btn-primary btn-block" href="yourflight" role="button">Your Flight</a>
			</div><!-- /col -->
			<div class="my-1 col-sm-3 col-lg-2">
				<a class="btn btn-primary btn-block" role="button">Confirmation</a>
			</div><!-- /col -->
			<div class="my-1 col-sm-3 col-lg-2">
				<a class="btn btn-secondary btn-block" role="button">Print&nbsp;PDF</a>
			</div><!-- /col -->
		</div><!-- /row -->

		<div class="thinblock">
			<p class="review">
				<label class="form_label" for="form-review_code_code" id="form-review_code_code_label">Your Application</label>
				 maytest0021
				<input id="form-review_code_code" name="form-review_code_code" value="maytest0021" type="hidden">
			</p>	
		</div>
		<div class="thinblock">
		<p class="review"><label class="form_label" for="form-review_details_name" id="form-review_details_name_label">Name</label> Mr ffdd dffd<input id="form-review_details_name_field_title" name="form-review_details_name_field_title" value="1" type="hidden"><input id="form-review_details_name_field_name" name="form-review_details_name_field_name" value="ffdd" type="hidden"><input id="form-review_details_name_field_surname" name="form-review_details_name_field_surname" value="dffd" type="hidden"></p><p class="review"><label class="form_label" for="form-review_details_email" id="form-review_details_email_label">Email</label> dds@dsds.com<input id="form-review_details_email" name="form-review_details_email" value="dds@dsds.com" type="hidden"></p>	</div>
		<div class="thinblock">
		<p class="review"><label class="form_label" for="form-review_departure_airport" id="form-review_departure_airport_label">Departure airport</label> Singapore<input id="form-review_departure_airport" name="form-review_departure_airport" value="SIN" type="hidden"></p>	</div>
		<div class="thinblock">
		<p class="review"><label class="form_label" for="form-review_destinations_airport_field1" id="form-review_destinations_airport_field1_label">Destination</label> Bangkok<input id="form-review_destinations_airport_field1" name="form-review_destinations_airport_field1" value="BKK" type="hidden"></p><p class="review"><label class="form_label" for="form-review_destinations_airport_field2" id="form-review_destinations_airport_field2_label">Destination</label> Cebu<input id="form-review_destinations_airport_field2" name="form-review_destinations_airport_field2" value="CEB" type="hidden"></p><p class="review"><label class="form_label" for="form-review_destinations_airport_field3" id="form-review_destinations_airport_field3_label">Destination</label> Manila<input id="form-review_destinations_airport_field3" name="form-review_destinations_airport_field3" value="MNL" type="hidden"></p>	</div>
		<div class="thinblock">
		<p class="review"><label class="form_label" for="form-review_flights_date_field1" id="form-review_flights_date_field1_label">Travel Date</label> 11.09.2020 - 3 nights</p><p class="review"><label class="form_label" for="form-review_flights_date_field2" id="form-review_flights_date_field2_label">Travel Date</label> 02.02.2020 - 11 nights</p><p class="review"><label class="form_label" for="form-review_flights_date_field3" id="form-review_flights_date_field3_label">Travel Date</label> 12.10.2020 - 14 nights</p>	</div>
			<button class="btn btn-danger btn-lg my-3" name="form-code-login_form-submitted" value="go">
			Submit	
		</button>
	</form>
@endsection

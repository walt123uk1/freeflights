@extends('layouts.app') 
@section('content')
<h2>Your Flight</h2>
<p>Please enter your departure airport, three destinations and three possible travel dates here.</p>
<p>Please note that no travel date or destination will take priority over another.</p>
<p>All travel (departure and return) must be completed by 30.09.2020.</p>
<p>Please note that all applications have to be received at least 45 days before the desired first travel date. All travel dates chosen must be 30 days apart from each other.</p>
<form id="form-details" method="post" action="/review" enctype="multipart/form-data" accept-charset="UTF-8" class="responsive">
		{{ csrf_field() }}	
		
	<input type="hidden" name="form-name" value="form-details">
	<input name="form-details_form-submitted" id="form-details_form-submitted" type="hidden">
	<div class="button-row row">
		<div class="my-1 col-sm-3  offset-lg-2 col-lg-2">
			<a class="btn btn-primary btn-block" href="/aboutyou" role="button">About You</a>
		</div>
		<!-- /col -->
		<div class="my-1 col-sm-3 col-lg-2">
			<a class="btn btn-primary btn-block" role="button">Your Flight</a>
		</div>
		<!-- /col -->
		<div class="my-1 col-sm-3 col-lg-2">
			<a class="btn btn-secondary btn-block" role="button">Confirmation</a>
		</div>
		<!-- /col -->
		<div class="my-1 col-sm-3 col-lg-2">
			<a class="btn btn-secondary btn-block" role="button">Print&nbsp;PDF</a>
		</div>
		<!-- /col -->
	</div>
	<!-- /row -->
	<div>
		<h3>My departure airport:</h3>
		<div class="form-group row">
			<div class="offset-sm-3 col-sm-6">
				<select class="form-control" id="exampleFormControlSelect1">
					<option>Departure airport</option>
					<option value="SIN">Singapore</option>
				</select>
			</div>
		</div>
		<h3>My destinations:</h3>
		<p>Please choose three different destinations. Please note that no destination will take&nbsp;priority over another.</p>
		<div class="form-group row">
			<div class="offset-sm-3 col-sm-6">
				<select class="form-control" id="exampleFormControlSelect1">
					<option>Departure airport</option>
					<option value="SIN">Singapore</option>
				</select>
			</div>
		</div>
		<div class="form-group row">
			<div class="offset-sm-3 col-sm-6">
				<select class="form-control" id="exampleFormControlSelect1">
					<option>Departure airport</option>
					<option value="SIN">Singapore</option>
				</select>
			</div>
		</div>
		<div class="form-group row">
			<div class="offset-sm-3 col-sm-6">
				<select class="form-control" id="exampleFormControlSelect1">
					<option>Departure airport</option>
					<option value="SIN">Singapore</option>
				</select>
			</div>
		</div>
		<label id="form-flights_flights_destination_field3_report" class="report"></label>
		<h3>My preferred travel dates are:</h3>
		<p>Please enter three different dates on which you can travel. Dates must be at least 45 days after the date of your application. Your three travel dates need to be at least 30 days apart from each other.&nbsp;Please note that no&nbsp;travel date will take priority over another and travel dates are not matched to any destination.</p>	
		<div class="form-group row">
			<div class="offset-sm-2 col-sm-4">
				<label for="alex">Departure Date </label>
				<input type="text" id="alex" name="alex">
			</div>
			<div class="col-sm-4">
				<label for="barry">Return Date</label>
				<input type="text" id="barry" name="barry">
			</div>
		</div>
		<div class="form-group row">
			<div class="offset-sm-2 col-sm-4">
				<label for="cory">Departure Date </label>
				<input type="text" id="cory" name="cory">
			</div>
			<div class="col-sm-4">
				<label for="dave">Return Date</label>
				<input type="text" id="dave" name="dave">
			</div>
		</div>
		<div class="form-group row">
			<div class="offset-sm-2 col-sm-4">
				<label for="errol">Departure Date </label>
				<input type="text" id="errol" name="errol">
			</div>
			<div class="col-sm-4">
				<label for=frank>Return Date</label>
				<input type="text" id="frank" name="frank">
			</div>
		</div>
	<button class="btn btn-danger btn-lg my-3" name="form-code-login_form-submitted" value="go">
		Submit	
	</button>
</form>
@endsection

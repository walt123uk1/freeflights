@extends('layouts.app') 
@section('content')
<h2>Thank you!</h2>
<p>Thank you for completing your application.
<br />
<br />Now please download your PDF application to your computer.
</p>
<form id="form-code-login" action="/thanks" method="post" enctype="multipart/form-data" accept-charset="UTF-8" class="responsive">
	{{ csrf_field() }}	
	<input type="hidden" name="form-name" value="form-details">
	<input name="form-details_form-submitted" id="form-details_form-submitted" type="hidden">
	<div class="button-row row">
		<div class="my-1 col-sm-3  offset-lg-2 col-lg-2">
			<a class="btn btn-primary  btn-block" href="aboutyou" role="button">About You</a>
		</div>
		<!-- /col -->
		<div class="my-1 col-sm-3 col-lg-2">
			<a class="btn btn-primary btn-block" href="yourflight" role="button">Your Flight</a>
		</div>
		<!-- /col -->
		<div class="my-1 col-sm-3 col-lg-2">
			<a class="btn btn-primary btn-block" href="review" role="button">Confirmation</a>
		</div>
		<!-- /col -->
		<div class="my-1 col-sm-3 col-lg-2">
			<a class="btn btn-primary btn-block" role="button">Print&nbsp;PDF</a>
		</div>
		<!-- /col -->
	</div>
	<!-- /row -->
	<button class="btn btn-danger btn-lg my-3" name="form-code-login_form-submitted" value="go">
		Download PDF	
	</button>
</form>
<p>In order to claim your free return flight, you will need to print out your application form (PDF) and post it unregistered together with the proof of purchase (showing the date of purchase, product description and store name) to:
<br />
<br />
<strong>Inspire -Rix </strong>
<br />
<strong>Fly with Candy</strong>
<br />Regus Sinop Center, 4th floor, 
<br />Sinopskaya naberezhnaya, 22, 
<br />Saint Petersburg, 191167 Russia 
</p>
<p>Please note that The Organizer cannot accept any responsibility for lost or delayed documents. 
	<br />Please ensure you make copies of your documents before posting them.
</p>
<p>Please note postal mail can take up several days to be delivered. Please check your emails and spam inbox regularly once you have posted your application. </p>
<p>All claims must be completed and posted within the redemption window between August 29 and September 8, 2019. Claims posted after this term shall not participate and shall be excluded from the promotion.</p>
<p>The second step requires you to complete your personal details and tell us about any additional passengers you would like to take with you on your trip. You can add up to three additional passengers.</p>
<p>After completion of the second step, inspire will contact you within 14 days with an offer, based on a combination of your flight dates and destinations.</p>
@endsection

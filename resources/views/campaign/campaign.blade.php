<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <!-- Styles - this is a small proof of concept app
            Usually this code would be in separate resources/css/*.scss files NOT inline.
         -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 48px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .customers {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 60%;
                margin: 20px 20%;

            }
            #nav {
                float: right;
                margin: 20px 20%;

            }

            #customers td, #customers th {
                border: 1px solid #ddd;
                padding: 8px;
                text-align: left;
            }

            #customers tr:nth-child(even){background-color: #f2f2f2;}

            #customers tr:hover {background-color: #ddd;}

            #customers th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #4CAF50;
                color: white;
            }
            #customers th a{
                text-decoration: none;
                
                color: white;
            }    
            .contact{
            padding: 4%;
            height: 400px;
            }
            .col-md-3{
            background: #ff9b00;
            padding: 4%;
            border-top-left-radius: 0.5rem;
            border-bottom-left-radius: 0.5rem;
            }
            .contact-info{
            margin-top:10%;
            }
            .contact-info img{
            margin-bottom: 15%;
            }
            .contact-info h2{
            margin-bottom: 10%;
            }
            .col-md-9{
            background: #fff;
            padding: 3%;
            border-top-right-radius: 0.5rem;
            border-bottom-right-radius: 0.5rem;
            }
            .contact-form label{
            font-weight:600;
            }
            .contact-form button{
            background: #25274d;
            color: #fff;
            font-weight: 600;
            width: 25%;
            }
            .contact-form button:focus{
            box-shadow:none;
            }
        </style>
    </head>
    <body>
        <div class="position-ref full-height">

            <div class="content">
            <!-- Title -->
                <div class="title m-b-md">
                    Campaign Page
                </div>
                
                <div class="customers">
                <!-- To do display feedback message  to user -->
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    <br>
                    @endif
                    <!-- Create campaign form 5 basic fields -->
                    <form action="{{ route('campaign.create') }}" method="post" accept-charset="utf-8">
                        @csrf
                        <div class="contact-form">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="name">Name:</label>
                                <div class="col-sm-10">          
                                <input type="text" class="form-control" id="name" placeholder="Enter Name" value="{{old('name')}}" name="name">
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="prefix">Prefix:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="prefix" placeholder="Enter prefix" value="{{old('prefix')}}" name="prefix">
                                <span class="text-danger">{{ $errors->first('prefix') }}</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="journey">Journey:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="journey" placeholder="Enter journey" value="{{old('journey')}}" name="journey">
                                <span class="text-danger">{{ $errors->first('journey') }}</span>
                                </div>
                            </div>
                            <!-- Text input with js datepicker -->
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="Start Date">Start Date:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="startdate" placeholder="Enter Start Date" value="{{old('startdate')}}" name="startdate">     
                                <span class="text-danger">{{ $errors->first('startdate') }}</span>
                                </div>
                            </div>
                            <!-- HTML 5 Date input no need for datepicker in jquery ui or bootstrap -->
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="End Date">End Date:</label>
                                <div class="col-sm-10">
                                <input type="date" class="form-control" id="enddate" placeholder="Enter End Date" value="{{old('enddate')}}" name="enddate">
                                <span class="text-danger">{{ $errors->first('enddate') }}</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div>
                <!-- Sortable table  -->
                    <table id="customers" class="customers">
                        <tr>
                            <th>@sortablelink('campaign','Campaign')</th>
                            <th>@sortablelink('name','Name')</th>
                            <th>@sortablelink('journey','Journey')</th>
                            <th>@sortablelink('title','Title')</th>
                        </tr>
                        @foreach ($campaigns as $campaign )
                            <tr>
                                <td>{{$campaign->campaign}}</td>                                
                                <td>{{$campaign->name}}</td>
                                <td>{{$campaign->journey}}</td>
                                <td>{{$campaign->title}}</td>
                                <td>
                                <!-- Form around Delete button npte method is POST and DELETE -->
                                    <form action="{{ route('campaign.delete',$campaign->campaign) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>                            
                        @endforeach                      
                    </table>
                    <!-- By excluding the page the sort is not reset on page change-->
                    <div id="nav">
                        {!! $campaigns->appends(\Request::except(['page','_token']))->render() !!}  
                    </div>
                </div>
            </div>
        </div>
        <!-- Javascript - this is a small proof of concept app
            Usually this code would be in separate resources/js/*.js files NOT inline.
         -->

        <script type="text/javascript">
            $('#startdate').datepicker({
            dateFormat: 'dd/mm/yy',       
            })
        </script>
        <script type="text/javascript">
            // $('#enddate').datepicker({
            // dateFormat: 'dd/mm/yy',       
            // })
        </script>
    </body>
</html>

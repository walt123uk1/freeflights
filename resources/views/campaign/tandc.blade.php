@extends('layouts.app') 
@section('content')<h3>Terms and Conditions</h3>
	<p>1. “Fly with Candy” is promoted by CANDY CIS llc (Promoter). All bookings, service and customer communications are provided by RIX AG, a company registered in Switzerland (company number CH 320.3.052.688-6) having its registered office Diepoldsauerstrasse 20, 9443 Widnau / St. Gallen, Switzerland (Organizer).
		<br />
		<br />2. This promotional offer is only open to a person who is resident in Russia and is aged 18 or over, excluding employees and their immediate families of the Promoter, its agents and anyone professionally connected with the promotion. Only a shopper having made a purchase pursuant to these Terms and Conditions shall participate in this promotion (Promotion).
		<br />
		<br />3. Purchase, internet access and credit card to purchase companion ticket(s) or services not covered by this promotion may be required to participate.
		<br />
		<br />4. Subject to these Terms and Conditions, the Organizer shall provide a fare free return flight ticket including all airport taxes and duties to a shopper (the Participant) having made a Qualifying Purchase at participating stores during the promotional period from 10 September till 15 October, 2019.
		<br />
		<br />The participating Candy models: 
	</p>
	<div class="container-fluid">
		<div id="product-table" class="row justify-content-center">
			<div class="col-sm-8">
				<table class="table table-striped">	
					<tbody>
						<tr><td>Candy FCT615XL</td></tr>
						<tr><td>Candy FCT615NXL</td></tr>
						<tr><td>Candy FCT615WXL</td></tr>
						<tr><td>Candy FCP625XL/E</td></tr>
						<tr><td>Candy FCP625NXL/E</td></tr>
						<tr><td>Candy FCP625WXL/</td></tr>
						<tr><td>Candy FCP605XL/E1</td></tr>
						<tr><td>Candy FCP605NXL/E</td></tr>
						<tr><td>Candy FCP605WXL/E</td></tr>
						<tr><td>Candy FCP612X/E</td></tr>
						<tr><td>Candy FCP612N/E</td></tr>
						<tr><td>Candy FCP612W/E</td></tr>
						<tr><td>Candy FCC614GH/E</td></tr>
						<tr><td>Candy FCC614BA/E</td></tr>
						<tr><td>Candy FCC614BA/E</td></tr>
						<tr><td>Candy CLGC64SGBA</td></tr>
						<tr><td>Candy CLGC64SGGH</td></tr>
						<tr><td>Candy CHG6D4WGTWA</td></tr>
						<tr><td>Candy CHG6D4WGTGH</td></tr>
						<tr><td>Candy CHG6BF4WGTWA</td></tr>
						<tr><td>Candy CHG6BF4WGTGH</td></tr>
						<tr><td>Candy CHG6DWPX</td></tr>
						<tr><td>Candy CHG6BF4WPX</td></tr>
						<tr><td>Candy CSG64SWGX</td></tr>
						<tr><td>Candy CSG64SWGN</td></tr>
						<tr><td>Candy CSG64SWGB</td></tr>
						<tr><td>Candy CVG64SGX</td></tr>
						<tr><td>Candy CVG64STGN</td></tr>
						<tr><td>Candy CVG64STGB</td></tr>
						<tr><td>Candy CI640CB</td></tr>
						<tr><td>Candy CH64FC</td></tr>
						<tr><td>Candy CH64DXT</td></tr>
						<tr><td>Candy CH64CCB</td></tr>
						<tr><td>Candy CH64BFT</td></tr>
						<tr><td>Candy CH64CCW</td></tr>
						<tr><td>Candy CH64BVT</td></tr>
						<tr><td>Candy CH64DCT</td></tr>
						<tr><td>Candy CSS4 1282 D1/2-07</td></tr>
						<tr><td>Candy GVS4 137 TWC1/2-07</td></tr>
						<tr><td>Candy CSWS40 364 D/2-07</td></tr>
						<tr><td>Candy GVSW40 364 TWHC-07</td></tr>
						<tr><td>Candy GVF4 137 LWHC/2-07</td></tr>
						<tr><td>Candy CSS44 1282DB/2-07</td></tr>
						<tr><td>Candy SGV44 128 TWB3-07</td></tr>
						<tr><td>Candy GVF44 138 LWHC3-07</td></tr>
						<tr><td>Candy GVS44 138 TWHC-07</td></tr>
						<tr><td>Candy GVS44 128 TWC3-07</td></tr>
						<tr><td>Candy CS C9LG-07</td></tr>
						<tr><td>Candy CS4 H7A1DE-07/</td></tr>
						<tr><td>Candy CS C10DBGX-07</td></tr>
						<tr><td>Candy GSV C10DSGX-07</td></tr>
						<tr><td>Candy GVS 4137 TWC 1/2-07</td></tr>
						<tr><td>Candy SGV 44128 TWB 3-07</td></tr>
						<tr><td>Candy GVS 34116 TC 2/2-07</td></tr>
					</tbody>
				</table>
			</div>	
		</div>	
	</div>	
	<p>The participating stores are the following:
		<br /> - Technopark
		<br /> - PBT
		<br /> - Cholodilnik.ru
		<br /> - Evropa
		<br />
		<br />The Participant must keep the sales receipt for the Purchase as proof of Purchase. Sales receipts cannot be transferred to third parties or used in someone else’s name.
		<br />
		<br />5. All claims must be completed and posted within October 21-27, 2019. Claims posted after the expiration of the above term shall not participate and shall be excluded from the promotion.
		<br />
		<br />6. To claim the Participant shall log in to www.flywithcandy.ru (promo-site) by using the corresponding sales receipt number and complete the online application form with their name, email address and their requested point of departure, their three requested different destinations and their three requested dates for departure and return. The departure dates have to be in different months and 30 days apart. The requested travel dates must be at least 45 calendar days in advance from the date the listed below form is posted.
		<br />
		<br />7. The Participant shall complete the form via the promo-site, then print out the completed form and post it to the Organizer together with the sales receipt and invoice copy (showing the purchase date and product description) by the mail to the promotional PO Box. The address shall be found on the completed Application form.
		<br />
		<br />8. On receipt of the customer Application, the Organizer within 7 calendar days shall validate the applicant and sales receipt data and if successful, email the participant a confirmation code.
		<br />
		<br />9. Within 7 calendar days from the receipt of the confirmation code the Participant should visit the campaign promo-page and complete filling out the Application, ie. enter personal data, including passport data, upload the scan of the front page of the travel passport and submit this Application to the Organizer online.
		<br />
		<br />10. Concurrently with filling-out their Application, the Participant may add to this Application up to 3 companion passengers and enter therein personal data and the passport detail of all other passengers flying.
		<br />
		<br />11. The Participant will receive by email a flight offer from RIX within the next 14 calendar days. The Participant should accept or reject the flight offer by return email within 48 hours after receiving the offer. The Participant’s rejecting the flight offer shall terminate the participation in the Promotion. 
		<br />
		<br />12. By accepting the above flight offer the Participant may also accept the price offer in respect of any of companion passengers included in the same Application and listed in the flight offer. The Participant will pay companion ticket(s) with a credit card in a special manner.
		<br />
		<br />13. Price and availability of the flight offered are not guaranteed until the offer is accepted. The offer cannot be guaranteed if not confirmed within 48 hours.
		<br />
		<br />14. Subject to receiving confirmation from the Participant the Organizer will make the flight booking and send booking confirmation (e-ticket) to the Participant. It is the Participant’s responsibility to check the confirmation details immediately and inform the Organizer within 12 hours of receipt should the confirmation details be incorrect. No cancellations or changes will be accepted thereafter. The flight ticket is neither transferable nor refundable. 
		<br />
		<br />15. All travel (departure and return) must be completed by August 31, 2020 and is dependent on the carriers’ available flight schedules. This promotional offer is not valid for journeys during peak periods and over, for instance, New Year or May Holidays. Some periods (e.g. religious and school holidays) and dates may be restricted depending on the chosen destination and travel time. The availability of flights may also be restricted by special events, trade fairs or local holidays.
		<br />
		<br />16. Subject these Terms and Conditions, the Participant will be issued a free economy-class return flight for one passenger, including taxes &amp; charges (one free flight ticket). The Participant will be paid for, via redemption of this promotion only, the participant’s carrier base fare, taxes, passenger duty, fuel surcharges, service charge and airport fee. All other costs associated with the flight will be borne by the Participant and are excluded from the promotional offer. Such other costs include, but are not limited to (i) any baggage or other charges made in connection with the flight or in addition to the fare (ii) any insurance (iii) any additional fees for card payments (iv) any fees for hotel, car hire, airport transfers, taxis, car park, seat upgrade, food and drink, visas, immigration documents or any other additional services.
		<br />
		<br />17.The following points of destinations are available from:
	</p>
	<div class="container-fluid">
		<div id="flight-table" class="row justify-content-center">
			<div class="col-sm-4 col-lg-2">
				<table class="table table-striped">
					<thead class="thead-dark">
						<tr><th>Moscow</th></tr>
					</thead>	
					<tbody>
						<tr><td>Amsterdam</td></tr>
						<tr><td>Barcelona</td></tr>
						<tr><td>Berlin</td></tr>
						<tr><td>Brussels</td></tr>
						<tr><td>Budapest</td></tr>
						<tr><td>Copenhagen</td></tr>
						<tr><td>Frankfurt</td></tr>
						<tr><td>Helsinki</td></tr>
						<tr><td>Istanbul</td></tr>
						<tr><td>London</td></tr>
						<tr><td>Minsk</td></tr>
						<tr><td>Milan</td></tr>
						<tr><td>Paris</td></tr>
						<tr><td>Prague</td></tr>
						<tr><td>Riga</td></tr>
						<tr><td>Rome</td></tr>
						<tr><td>Adler - Sochi</td></tr>
						<tr><td>Simferopol</td></tr>
						<tr><td>Stockholm</td></tr>
						<tr><td>Warsaw</td></tr>
						<tr><td>Vienna</td></tr>
						<tr><td>Zurich</td></tr>
					</tbody>
				</table>
			</div>		
			<div class="col-sm-4 col-lg-2">
				<table class="table table-striped">
					<thead class="thead-dark">
						<tr><th>Saint-Petersburg</th></tr>
					</thead>
					<tbody>
						<tr><td>Amsterdam</td></tr>
						<tr><td>Barcelona</td></tr>
						<tr><td>Berlin</td></tr>
						<tr><td>Brussels</td></tr>
						<tr><td>Budapest</td></tr>
						<tr><td>Copenhagen</td></tr>
						<tr><td>Frankfurt</td></tr>
						<tr><td>Helsinki</td></tr>
						<tr><td>Istanbul</td></tr>
						<tr><td>London</td></tr>
						<tr><td>Minsk</td></tr>
						<tr><td></td></tr>
						<tr><td>Paris</td></tr>
						<tr><td>Prague</td></tr>
						<tr><td>Riga</td></tr>
						<tr><td>Rome</td></tr>
						<tr><td>Adler - Sochi</td></tr>
						<tr><td>Simferopol</td></tr>
						<tr><td>Stockholm</td></tr>
						<tr><td>Warsaw</td></tr>
						<tr><td>Vienna</td></tr>
						<tr><td>Zurich</td></tr>
					</tbody>
				</table>
			</div>	
			<div class="col-sm-4 col-lg-2">
				<table class="table table-striped">
					<thead class="thead-dark">
					<tr><th>Minsk</th></tr>
					</thead>
					<tbody>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td>Berlin</td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td>London</td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td>Paris</td></tr>
						<tr><td>Prague</td></tr>
						<tr><td>Riga</td></tr>
						<tr><td></td></tr>
						<tr><td>Adler - Sochi</td></tr>
						<tr><td>Simferopol</td></tr>
						<tr><td></td></tr>
						<tr><td>Warsaw</td></tr>
						<tr><td></td></tr>
						<tr><td>Zurich</td></tr>
					</tbody>
				</table>			
			</div>
			<div class="col-sm-4 col-lg-2">
				<table class="table table-striped">
					<thead class="thead-dark">
						<tr><th>Bramhall</th></tr>
					</thead>
					<tbody>
						<tr><td>Amsterdam</td></tr>
						<tr><td></td></tr>
						<tr><td>Berlin</td></tr>
						<tr><td>Brussels</td></tr>
						<tr><td></td></tr>
						<tr><td>Copenhagen</td></tr>
						<tr><td>Frankfurt</td></tr>
						<tr><td>Helsinki</td></tr>
						<tr><td></td></tr>
						<tr><td>London</td></tr>
						<tr><td>Minsk</td></tr>
						<tr><td></td></tr>
						<tr><td>Paris</td></tr>
						<tr><td>Prague</td></tr>
						<tr><td>Riga</td></tr>
						<tr><td>Rome</td></tr>
						<tr><td>Adler - Sochi</td></tr>
						<tr><td></td></tr>
						<tr><td>Stockholm</td></tr>
						<tr><td>Warsaw</td></tr>
						<tr><td></td></tr>
						<tr><td>Zurich</td></tr>
					</tbody>
				</table>	
			</div>	
			<div class="col-sm-4 col-lg-2">
				<table class="table table-striped">
					<thead class="thead-dark">
						<tr><th>Lisbon</th></tr>
					</thead>
					<tbody>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td>Berlin</td></tr>
						<tr><td>Brussels</td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td>London</td></tr>
						<tr><td>Minsk</td></tr>
						<tr><td></td></tr>
						<tr><td>Paris</td></tr>
						<tr><td>Prague</td></tr>
						<tr><td>Riga</td></tr>
						<tr><td>Rome</td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td>Warsaw</td></tr>
						<tr><td></td></tr>
						<tr><td>Zurich</td></tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<p>18. The airport of departure is dependent on the destination, and not all destinations are available from all listed points of departures. The availability of flights may be limited depending on the chosen destination or travel time, and the Organizer will offer the Participant an alternate option. The choice of carrier is at the sole discretion of the Organizer. The Participant is not entitled to demand a particular carrier or route. The minimum stay is 2 days and the maximum stay is 30 calendar days.</p>
	<p>19. All flights are subject to booking availability. This promotional offer does not entitle the Participant to travel via a particular route, carrier or on a particular date and you may be offered an alternative destination or travel date to the ones you requested.</p>
	<p>20. The Promoter does not accept liability for changes to flight schedules, routes or destinations and reserves the right to use other service providers as an alternative. In the event of war, earthquake, disaster or similar acts of god, or otherwise where there are circumstances wholly outside the reasonable control of the Promoter and only where circumstances make this unavoidable, the Promoter reserves the right to withdraw this promotion without notice and without offering compensation.</p>
	<p>21. The right of the Participant and any person travelling with the participant to claim compensation is subject to legal and carrier regulatory provisions. The Promoter and the Organizer are not liable for compensation.</p>
	<p>22. Each free flight application is limited for a single use. The right of a free flight is valid for one person, booking and return flight. Free flights cannot be used in combination with each other.</p>
	<p>23. The Participant will not be entitled to claim if the promotional product is returned.</p>
	<p>24. Bookings cannot be requested or made for by third parties and flights may not be booked by third parties using the name of the participant.</p>
	<p>25. The Promoter, through the Organizer reserves the right to request the original proof of purchase from the Participant. Furthermore, the Promoter, through the Organizer reserves the right to verify all claims, check the content of all applications and registrations to ensure the Participant’s compliance with these Terms &amp; Conditions and to refuse to award a flight or withdraw flight entitlement where there are reasonable grounds to believe there has been a breach of these terms and conditions or any instructions forming part of this promotions entry requirements or otherwise where a Participant has gained unfair advantage in participating in the offer or claimed using fraudulent means. Any requested original proof of purchase will be returned to the Participant by post.</p>
	<p>26. Personal data captured during the course of the promotion will be processed in accordance with applicable data protection laws and regulations. No personal data shall be passed to third parties except where these are required for booking and fulfilment purposes.</p>
	<p>27. The Promoter and the Organizer will not accept any responsibility for claims lost and delayed or for those claims not received by the application cut-off. It is your responsibility to ensure that correct details are provided for claims to be processed. The Promoter and the Organizer will not be liable for technical malfunction to the promotion website or any traffic congestion or internet inaccessibility. The Promoter does not warrant that the promotion website will be uninterrupted or error free.</p>
	<p>28. By participating in this promotion the Participant will be deemed to have accepted these Terms and Conditions. The Promoter reserves the right to amend the Terms and Conditions without notice.</p>
	<p>29. All written correspondence should be sent at the address indicated at the promo site www.flywithcandy.ru. Participants who have queries or complaints about the promotion can e-mail the Organizer or alternatively call the Organizer using the contact details on the promotion website.</p>
@endsection

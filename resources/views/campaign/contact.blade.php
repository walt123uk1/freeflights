@extends('layouts.app') 
@section('content')	
<div class="content">
	<input type="hidden" name="form-name" value="form-contact"/>
	<input name="form-contact_form-submitted" id="form-contact_form-submitted" type="hidden"/>
	<h2>Contact Form</h2>
	<p>Please complete the contact form below with your details and your message and we will get back to you as soon as possible.</p>
	<p>You can also contact us on 81080026015011, Monday – Friday, 10am - 5pm (Moscow time).</p> 			
	<div class="col-sm-12">
		<p>Required&nbsp;fields are marked with a <span class="required">*</span></p>	
	</div>	
	<div class="form-group row">
		<label for="name" class="offset-sm-2 col-sm-2 col-form-label">*Full Name</label>
		<div class="col-sm-6">
			<input type="text" class="form-control" id="name" placeholder="Full Name">
		</div>
	</div>
	<div class="form-group row">
		<label class="offset-sm-2 col-sm-2 col-form-label" for="form-contact_phone" id="form-contact_phone_label">*Phone number</label>
		<div class="col-sm-6">
			<input type="text"  class="form-control" id="form-contact_phone" placeholder="Phone Number">
		</div>
	</div>
	<div class="form-group row">
		<label class="offset-sm-2 col-sm-2 col-form-label" for="form-contact_email" id="form-contact_email_label">*Email</label>
		<div class="col-sm-6">
			<input type="email" class="form-control" id="form-contact_email" placeholder="Email">
		</div>
	</div>
	<div class="form-group row">
		<label class="offset-sm-2 col-sm-2 col-form-label" for="form-contact_message" id="form-contact_message_label">*My Message</label>
		<div class="col-sm-6">
			<textarea class="form-control" id="form-contact_message" rows="5"></textarea>
		</div>
	</div>
	<button class="btn btn-danger btn-lg" name="form-code-login_form-submitted" value="go">
		Submit	
	</button>
</div>	
@endsection

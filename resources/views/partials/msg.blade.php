@if (count($errors) >0)
  <div class="alert alert-danger" role="alert">
    <strong> Errors: </strong>
    <ul style="margin-left:3rem;">
      @foreach($errors->all() as $error)
        <li>{{$error }}</li>
      @endforeach
    </ul>
  </div>
@endif

@if (Session::has('create_user_success'))
  <div class="alert alert-success" role="alert">
    <strong> Success: </strong> {{ Session::get('create_user_success') }}
  </div>
@endif

@if (Session::has('update_user_success'))
  <div class="alert alert-success" role="alert">
    <strong> Success: </strong> {{ Session::get('update_user_success') }}
  </div>
@endif

@if (Session::has('update_user_profile'))
  <div class="alert alert-success" role="alert">
    <strong> Success: </strong> {{ Session::get('update_user_profile') }}
  </div>
@endif

@if (Session::has('create_client_contact'))
  <div class="alert alert-success" role="alert">
    <strong> Success: </strong> {{ Session::get('create_client_contact') }}
  </div>
@endif

@if (Session::has('update_client_contact'))
  <div class="alert alert-success" role="alert">
    <strong> Success: </strong> {{ Session::get('update_client_contact') }}
  </div>
@endif

@if (Session::has('archive_client_contact'))
  <div class="alert alert-success" role="alert">
    <strong> Success: </strong> {{ Session::get('archive_client_contact') }}
  </div>
@endif


@if (Session::has('upload_resource'))
  <div class="alert alert-success" role="alert">
    <strong> Success: </strong> {{ Session::get('upload_resource') }}
  </div>
@endif

@if (Session::has('create_client'))
  <div class="alert alert-success" role="alert">
    <strong> Success: </strong> {{ Session::get('create_client') }}
  </div>
@endif

@if (Session::has('update_client'))
  <div class="alert alert-success" role="alert">
    <strong> Success: </strong> {{ Session::get('update_client') }}
  </div>
@endif


@if (Session::has('create_news_success'))
  <div class="alert alert-success" role="alert">
    <strong> Success: </strong> {{ Session::get('create_news_success') }}
  </div>
@endif

@if (Session::has('update_news_success'))
  <div class="alert alert-success" role="alert">
    <strong> Success: </strong> {{ Session::get('update_news_success') }}
  </div>
@endif




@if (Session::has('verify_success'))

  <div class="alert alert-success" role="alert">
    <strong> Success: </strong> {{ Session::get('verify_success') }}
  </div>

@endif
<div>
<ul id="form-airline-grid" class="airlines">
	<li><img src="{{asset('images/aerolineas_argentinas.png')}}" alt="Aerolíneas Argentinas" title="Aerolíneas Argentinas"></li>
	<li><img src="{{asset('images/aeromexico.png')}}" alt="Aeroméxico" title="Aeroméxico"></li>
	<li><img src="{{asset('images/aircanada.png')}}" alt="Air Canada" title="Air Canada"></li>
	<li><img src="{{asset('images/airfrance.png')}}" alt="Air France" title="Air France"></li>
	<li><img src="{{asset('images/americanairlines.png')}}" alt="American Airlines" title="American Airlines"></li>
	<li><img src="{{asset('images/avianca.png')}}" alt="Avianca - Aerovías del Continente Americano S.A." title="Avianca - Aerovías del Continente Americano S.A."></li>
	<li><img src="{{asset('images/azul.png')}}" alt="Azul Linhas Aéreas Brasileiras" title="Azul Linhas Aéreas Brasileiras"></li>
	<li><img src="{{asset('images/boliviana_de_aviacion.png')}}" alt="Boliviana de Aviación" title="Boliviana de Aviación"></li>
	<li><img src="{{asset('images/cathay_pacific.png')}}" alt="Cathay Pacific" title="Cathay Pacific"></li>
	<li><img src="{{asset('images/copa_airlines.png')}}" alt="Copa Airlines" title="Copa Airlines"></li>
	<li><img src="{{asset('images/delta.png')}}" alt="Delta Air Lines" title="Delta Air Lines"></li>
	<li><img src="{{asset('images/gol.png')}}" alt="Gol Transportes Aéreos" title="Gol Transportes Aéreos"></li>
	<li><img src="{{asset('images/latam.png')}}" alt="LATAM Airlines Group S.A." title="LATAM Airlines Group S.A."></li>
	<li><img src="{{asset('images/star_alliance.png')}}" alt="Star Alliance" title="Star Alliance"></li>
</ul>
</div>


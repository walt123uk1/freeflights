
  <div id="footer" class="row">
  <div class="offset-md-2 col-md-3">Powered by&nbsp;
          <a href="http://www.inspirewebsite.de">
            <img title="inspire" src="{{asset('images/inspire-logo_grey.png')}}" alt="inspire">
          </a>
        &amp; 
        <a href="http://rix.ag/en/rix-marketing/concept">
          <img class="rixlogo" title="Rix" src="{{asset('images/rix_logo_sml.png')}}" alt="Rix">
        </a>
        <a href="/aboutus" >
          About	
        </a>
  </div>
  <div class="offset-md-2 col-md-3">©Inspire, Bramhall, England 2018. All rights reserved</div>
</div>

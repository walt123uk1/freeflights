<!-- <nav class="nav sticky-top  nav-masthead justify-content-center">
        <a class="nav-link active" href="/">Flights</a>
        <a class="nav-link" href="/faqs">FAQS</a>
        <a class="nav-link" href="/tandc">Terms and Conditions</a>
        <a class="nav-link" href="contact">Contact</a>
</nav> -->

<nav class="navbar fixed-top navbar-light bg-white navbar-expand-md justify-content-center">
   <button class="navbar-toggler ml-1" type="button" data-toggle="collapse" data-target="#collapsingNavbar2">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="navbar-collapse collapse justify-content-between align-items-center w-100" id="collapsingNavbar2">
        <ul class="navbar-nav mx-auto text-center">
            <li class="nav-item active">
                <a class="nav-link active" href="/">Flights</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/faqs">FAQS</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/tandc">Terms and Conditions</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="contact">Contact</a>
            </li>   
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Language
                <span class="caret"></span></a>
                <ul class="dropdown-menu">
                <li class="nav-item"><a class="nav-link" href="#">Portuguese</a></li>
                <li class="nav-item"><a class="nav-link" href="#">English</a></li>
                </ul>
            </li>
        </ul>
</div>
</nav>
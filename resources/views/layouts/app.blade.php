<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('partials.head')
    <body class="text-center body-padding">
        <div class="overlay">
        </div>
        @include('partials.header')
        @include('partials.navigation')
        @include('partials.banner')
        <main class="inner cover">
            @include('partials.msg')
            @yield('content')
        </main>
        @include('partials.airline')
        @include('partials.footer')
        <script src="{{asset('js/app.js')}}"></script>
        @include('partials.scripts')
    </body>
</html>
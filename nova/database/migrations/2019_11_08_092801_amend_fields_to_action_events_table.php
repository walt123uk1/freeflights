<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AmendFieldsToActionEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Amends fields to accept varchar primary keys
        Schema::table('action_events', function (Blueprint $table) {
            $table->string('actionable_id', 50)->collate('utf8_unicode_ci')->change();
            $table->string('target_id', 50)->collate('utf8_unicode_ci')->change();
            $table->string('model_id', 20)->collate('utf8_unicode_ci')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('action_events', function (Blueprint $table) {
            //
        });
    }
}

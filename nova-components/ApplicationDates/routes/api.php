<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. You're free to add
| as many additional routes to this file as your tool may require.
|
*/
Route::get('/{resoureceId}', 'Twalton\ApplicationDates\Http\Controllers\Api@getDates');
Route::delete('/{resoureceId}/{sequence}', 'Twalton\ApplicationDates\Http\Controllers\Api@deleteDate');
Route::get('/edit/{code}/{sequence}/{date}/{stay}', 'Twalton\ApplicationDates\Http\Controllers\Api@editDate');
Route::get('/create/{code}/{sequence}/{date}/{stay}', 'Twalton\ApplicationDates\Http\Controllers\Api@createDate');

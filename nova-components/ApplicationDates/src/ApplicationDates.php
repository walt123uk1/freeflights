<?php

namespace Twalton\ApplicationDates;

use Laravel\Nova\ResourceTool;

class ApplicationDates extends ResourceTool
{
    /**
     * Get the displayable name of the resource tool.
     *
     * @return string
     */
    public function name()
    {
        return 'Application Dates';
    }

    /**
     * Get the component name for the resource tool.
     *
     * @return string
     */
    public function component()
    {
        return 'application-dates';
    }
}

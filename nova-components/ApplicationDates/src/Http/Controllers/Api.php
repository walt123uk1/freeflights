<?php
namespace Twalton\ApplicationDates\Http\Controllers;
use App\ApplicationDate;

class Api
{
    /**
     * Return all the registered routes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDates($code)
    {
        //dd($code);
        //$languages = CampaignLang::where('code',$request->resourceId)->get();
        $dates = ApplicationDate::where('code','=',$code)->get();
        return response()->json($dates)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function deleteDate($code,$sequence){        
        $date = ApplicationDate::where('code','=',$code)->where('sequence','=',$sequence)->delete();
        if($date) {
            return response()->json(['status' => 'success']);
        }
    }

    public function editDate($code,$sequence,$date,$stay)
    {                
        $record = ApplicationDate::where('code','=',$code)->where('sequence','=',$sequence)
        ->limit(1)
        ->update([
            'date' => $date,
            'stay' => $stay
        ]);
        if($record) {
            return response()->json(['status' => 'success']);
        }
    }

    public function createDate($code,$sequence,$date,$stay)
    {          
        $date_new = new ApplicationDate;
        $date_new->code = $code;
        $date_new->date = $date;
        $date_new->sequence = $sequence;
        $date_new->stay = $stay;
        $date_new->save();
        if($date_new) {
            return response()->json(['status' => 'success']);
        }
    }

}
<?php
namespace Twalton\OfferPassenger\Http\Controllers;
use App\OfferPassenger;
use Illuminate\Support\Str;
use Laravel\Nova\Http\Requests\NovaRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Log;

class Api
{
    /**
     * Return all the registered routes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPassengers($offer)
    {
        //dd($offer);
        $passenger = OfferPassenger::where('offer','=',$offer)->get();
        Log::debug($passenger);
        return response()->json($passenger)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function deletePassenger($offer,$sequence){        
        $sequence = OfferPassenger::where('offer','=',$offer)->where('sequence','=',$sequence)->delete();
        if($sequence) {
            return response()->json(['status' => 'success']);
        }
    }

    public function editPassenger($offer,$sequence)
    {                
        $record = OfferPassenger::where([['offer','=',$offer],['sequence','=',$sequence]])
        ->limit(1);
        // ->update(['alternate' => $alternate, 
        //'sequence'=> $sequence
        //]);
        if($record) {
            return response()->json(['status' => 'success']);
        }
    }

    public function createPassenger(Request $request)
    {       
        $passenger_offer_new = new OfferPassenger;
        $passenger_offer_new->offer = $request->offer;
        $passenger_offer_new->sequence = $request->sequence;
        $passenger_offer_new->title = $request->title;
        $passenger_offer_new->first_name = $request->first_name;
        $passenger_offer_new->last_name = $request->last_name;
        $passenger_offer_new->dob = $request->dob;
        $passenger_offer_new->type = $request->type;
        $passenger_offer_new->passport = $request->passport;
        $passenger_offer_new->passport_file = $request->passport_file;
        $passenger_offer_new->save();
        if($passenger_offer_new) {
            return response()->json(['status' => 'success']);
        }
    }
}
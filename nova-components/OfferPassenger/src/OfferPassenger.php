<?php

namespace Twalton\OfferPassenger;

use Laravel\Nova\ResourceTool;

class OfferPassenger extends ResourceTool
{
    /**
     * Get the displayable name of the resource tool.
     *
     * @return string
     */
    public function name()
    {
        return 'Offer Passenger';
    }

    /**
     * Get the component name for the resource tool.
     *
     * @return string
     */
    public function component()
    {
        return 'offer-passenger';
    }
}

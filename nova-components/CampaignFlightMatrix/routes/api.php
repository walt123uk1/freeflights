<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. You're free to add
| as many additional routes to this file as your tool may require.
|
*/

// Route::get('/endpoint', function (Request $request) {
//     //
// });
Route::get('/{resoureceId}', 'Twalton\CampaignFlightMatrix\Http\Controllers\Api@getFlightMatrix');
Route::get('/airport/airports', 'Twalton\CampaignFlightMatrix\Http\Controllers\Api@getAirports');
Route::post('/write', 'Twalton\CampaignFlightMatrix\Http\Controllers\Api@postFlightMatrix');


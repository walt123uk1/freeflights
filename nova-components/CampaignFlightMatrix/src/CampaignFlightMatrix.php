<?php

namespace Twalton\CampaignFlightMatrix;

use Laravel\Nova\ResourceTool;

class CampaignFlightMatrix extends ResourceTool
{
    /**
     * Get the displayable name of the resource tool.
     *
     * @return string
     */
    public function name()
    {
        return 'Campaign Flight Matrix';
    }

    /**
     * Get the component name for the resource tool.
     *
     * @return string
     */
    public function component()
    {
        return 'campaign-flight-matrix';
    }
}

<?php
namespace Twalton\CampaignFlightMatrix\Http\Controllers;
use App\Airport;
use App\CampaignDepartAirport;
use App\CampaignDestinationAirport;
use Illuminate\Support\Str;
use Laravel\Nova\Http\Requests\NovaRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Log;


class Api
{
    /**
     * Return all the registered routes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFlightMatrix($campaign)
    {
        //dd($campaign);
        $airports = 'inspire-flights_airports';
        $deptable = 'inspire-flights_campaign_departure_airports';
        $desttable = 'inspire-flights_campaign_destination_airports';
        $depairs = DB::connection('mysql2')->table($deptable)->where($deptable.'.campaign', '=', $campaign)
        ->join($airports, $airports.'.iata', "=", $deptable.'.iata')
        ->select($deptable.'.iata',$deptable.'.sequence',$airports.'.name')
        ->get();
        //dd($depairs);
        for($i = 0;$i < count($depairs);$i++)
        {
            $jsonResult[$i]["iata"] = $depairs[$i]->iata;
            $jsonResult[$i]["sequence"] = $depairs[$i]->sequence;
            $jsonResult[$i]["name"] = $depairs[$i]->name;
            $iata = $depairs[$i]->iata;
            $jsonResult[$i]["dests"] = DB::connection('mysql2')->table($desttable)
            ->where($desttable.'.campaign', '=', $campaign)
            ->where($desttable.'.departure', '=', $iata)
            ->join($airports, $airports.'.iata', "=", $desttable.'.iata')
            ->select($desttable.'.iata',$desttable.'.sequence',$airports.'.name')
            ->get();
        }
        //$depairs = CampaignDepartAirport::where('campaign','=',$campaign)->get();
        return response()->json($jsonResult)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function postFlightMatrix(Request $request){
        // Delete existing Departure airports
        CampaignDepartAirport::where('campaign',$request->resource)->delete();
        // Delete existing Destination airports
        CampaignDestinationAirport::where('campaign',$request->resource)->delete();
        //Create Destination Airports for Departure - 
        $i=0;
        foreach ($request->matrix as $dept) {
            //Create Departure Airport 
            // if Blankthen we won't create of add to sequence ie delete
            if ($dept['iata']!='') {
                $i++;
                $dept_create = new CampaignDepartAirport;
                $dept_create->campaign = $request->resource;
                $dept_create->sequence = $i;
                $dept_create->iata = $dept['iata'];
                $dept_create->save();
            }    
            $j=0;
            foreach ($dept['dests'] as $dest) {
                $j++;
                // if Blank add to sequence $j but don't create record
                if ($dest['iata']!='' && $dept['iata']!='') {
                    $dept_create = new CampaignDestinationAirport;
                    $dept_create->campaign = $request->resource;
                    $dept_create->departure = $dept['iata'];
                    $dept_create->sequence = $j;
                    $dept_create->iata = $dest['iata'];
                    $dept_create->save();                
                }
            }
        }    
        return response()->json("well done");
    }    
    public function getAirports()
    {        
        //dd($search);
        $jsonResult = Airport::all();
        //dd($jsonResult);
        return response()->json($jsonResult)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }
}    
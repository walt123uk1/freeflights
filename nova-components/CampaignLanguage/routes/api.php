<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. You're free to add
| as many additional routes to this file as your tool may require.
|b68ee0c2ac8078df0300ab6dfed8f0aabc7ef1a0
*/

// Route::get('/endpoint', function (Request $request) {
//     //
// });

Route::get('/{resoureceId}', 'Twalton\CampaignLanguage\Http\Controllers\Api@getLanguages')->name('languages');
Route::delete('/{resoureceId}/{language}', 'Twalton\CampaignLanguage\Http\Controllers\Api@deleteLanguage');
Route::get('/edit/{campaign}/{language}/{alternate}/{sequence}', 'Twalton\CampaignLanguage\Http\Controllers\Api@editLanguage');
Route::get('/create/{campaign}/{language}/{alternate}/{sequence}', 'Twalton\CampaignLanguage\Http\Controllers\Api@createLanguage');
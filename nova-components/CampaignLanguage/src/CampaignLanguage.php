<?php

namespace Twalton\CampaignLanguage;

use Laravel\Nova\ResourceTool;

class CampaignLanguage extends ResourceTool
{
    /**
     * Get the displayable name of the resource tool.
     *
     * @return string
     */
    public function name()
    {
        return 'Campaign Language';
    }

    /**
     * Get the component name for the resource tool.
     *
     * @return string
     */
    public function component()
    {
        return 'campaign-language';
    }
}

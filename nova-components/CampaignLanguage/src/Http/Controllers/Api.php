<?php
namespace Twalton\CampaignLanguage\Http\Controllers;
use App\CampaignLang;
use Illuminate\Support\Str;
use Laravel\Nova\Http\Requests\NovaRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as FacadesRequest;

class Api
{
    /**
     * Return all the registered routes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLanguages($campaign)
    {
        //dd($campaign);
        //$languages = CampaignLang::where('campaign',$request->resourceId)->get();
        $languages = CampaignLang::where('campaign','=',$campaign)->get();
        return response()->json($languages)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }


    public function deleteLanguage($campaign,$language){        
        $language = CampaignLang::where('campaign','=',$campaign)->where('language','=',$language)->delete();
        if($language) {
            return response()->json(['status' => 'success']);
        }
    }
    

    public function editLanguage($campaign,$language,$alternate,$sequence)
    {                
        if ($alternate=='') {$alternate=0;}
        $record = CampaignLang::where([['campaign','=',$campaign],['language','=',$language]])
        ->limit(1)
        ->update(['alternate' => $alternate, 
        'sequence'=> $sequence
        ]);
        if($record) {
            return response()->json(['status' => 'success']);
        }
    }

    public function createLanguage($campaign,$language,$alternate,$sequence)
    {          
        $lang_new = new CampaignLang;
        $lang_new->campaign = $campaign;
        $lang_new->language = $language;
        $lang_new->alternate = $alternate;
        $lang_new->sequence = $sequence;
        $lang_new->save();
        if($lang_new) {
            return response()->json(['status' => 'success']);
        }
    }
}
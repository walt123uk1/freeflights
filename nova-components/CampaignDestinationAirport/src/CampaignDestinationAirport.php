<?php

namespace Twalton\CampaignDestinationAirport;

use Laravel\Nova\ResourceTool;

class CampaignDestinationAirport extends ResourceTool
{
    /**
     * Get the displayable name of the resource tool.
     *
     * @return string
     */
    public function name()
    {
        return 'Campaign Destination Airport';
    }

    /**
     * Get the component name for the resource tool.
     *
     * @return string
     */
    public function component()
    {
        return 'campaign-destination-airport';
    }
}

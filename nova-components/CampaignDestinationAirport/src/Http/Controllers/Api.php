<?php
namespace Twalton\CampaignDestinationAirport\Http\Controllers;
use App\CampaignDestinationAirport;

class Api
{
    /**
     * Return all the registered routes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDestairs($campaign)
    {
        //dd($campaign);
        //$languages = CampaignLang::where('campaign',$request->resourceId)->get();
        $destairs = CampaignDestinationAirport::where('campaign','=',$campaign)->get();
        return response()->json($destairs)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }


    public function deleteDestair($campaign,$departure,$sequence){        
        $destair = CampaignDestinationAirport::where('campaign','=',$campaign)->where('departure','=',$departure)->where('sequence','=',$sequence)->delete();
        if($destair) {
            return response()->json(['status' => 'success']);
        }
    }
    

    public function editDestair($campaign,$departure,$sequence,$iata)
    {                
        $record = CampaignDestinationAirport::where('campaign','=',$campaign)->where('departure','=',$departure)->where('sequence','=',$sequence)
        ->limit(1)
        ->update(['iata' => $iata ]);
        if($record) {
            return response()->json(['status' => 'success']);
        }
    }

    public function createDestair($campaign,$departure,$sequence,$iata)
    {          
        $destair_new = new CampaignDestinationAirport;
        $destair_new->campaign = $campaign;
        $destair_new->iata = $iata;
        $destair_new->sequence = $sequence;
        $destair_new->departure = $departure;
        $destair_new->save();
        if($destair_new) {
            return response()->json(['status' => 'success']);
        }
    }
}
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. You're free to add
| as many additional routes to this file as your tool may require.
|
*/
Route::get('/{resoureceId}', 'Twalton\CampaignDestinationAirport\Http\Controllers\Api@getDestairs')->name('depairs');
Route::delete('/{resoureceId}/{departure}/{sequence}', 'Twalton\CampaignDestinationAirport\Http\Controllers\Api@deleteDestair');
Route::get('/edit/{campaign}/{departure}/{sequence}/{iata}', 'Twalton\CampaignDestinationAirport\Http\Controllers\Api@editDestair');
Route::get('/create/{campaign}/{departure}/{sequence}/{iata}', 'Twalton\CampaignDestinationAirport\Http\Controllers\Api@createDestair');

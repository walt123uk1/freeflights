<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. You're free to add
| as many additional routes to this file as your tool may require.
|
*/
Route::get('/{resoureceId}', 'Twalton\ApplicationStageLog\Http\Controllers\Api@getStageLogs');
Route::delete('/{resoureceId}/{sequence}', 'Twalton\ApplicationStageLog\Http\Controllers\Api@deleteStageLog');
Route::get('/edit/{code}/{sequence}/{destination}', 'Twalton\ApplicationStageLog\Http\Controllers\Api@editStageLog');
Route::get('/create/{code}/{sequence}/{destination}', 'Twalton\ApplicationStageLog\Http\Controllers\Api@createStageLog');

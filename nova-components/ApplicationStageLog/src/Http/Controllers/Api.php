<?php
namespace Twalton\ApplicationStageLog\Http\Controllers;
use App\ApplicationStageLog;

class Api
{
    /**
     * Return all the registered routes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStageLogs($code)
    {
        //dd($code);
        //$languages = CampaignLang::where('code',$request->resourceId)->get();
        $dests = ApplicationStageLog::where('code','=',$code)->get();
        return response()->json($dests)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function deleteStageLog($code,$date){        
        $dest = ApplicationStageLog::where('code','=',$code)->where('date','=',$date)->delete();
        if($dest) {
            return response()->json(['status' => 'success']);
        }
    }

    public function editStageLog($code,$date,$stage)
    {                
        $record = ApplicationStageLog::where('code','=',$code)->where('date','=',$date)
        ->limit(1)
        ->update([
            'stage' => $stage
        ]);
        if($record) {
            return response()->json(['status' => 'success']);
        }
    }

    public function createStageLog($code,$date,$stage)
    {          
        //dd($code);
        $dest_new = new ApplicationStageLog;
        $dest_new->code = $code;
        $dest_new->stage = $stage;
        $dest_new->date = $date;
        $dest_new->save();
        if($dest_new) {
            return response()->json(['status' => 'success']);
        }
    }

}
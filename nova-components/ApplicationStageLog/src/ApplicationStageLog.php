<?php

namespace Twalton\ApplicationStageLog;

use Laravel\Nova\ResourceTool;

class ApplicationStageLog extends ResourceTool
{
    /**
     * Get the displayable name of the resource tool.
     *
     * @return string
     */
    public function name()
    {
        return 'Application Stage Log';
    }

    /**
     * Get the component name for the resource tool.
     *
     * @return string
     */
    public function component()
    {
        return 'application-stage-log';
    }
}

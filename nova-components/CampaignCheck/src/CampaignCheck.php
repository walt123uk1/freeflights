<?php

namespace Twalton\CampaignCheck;

use Laravel\Nova\ResourceTool;

class CampaignCheck extends ResourceTool
{
    /**
     * Get the displayable name of the resource tool.
     *
     * @return string
     */
    public function name()
    {
        return 'Campaign Check';
    }

    /**
     * Get the component name for the resource tool.
     *
     * @return string
     */
    public function component()
    {
        return 'campaign-check';
    }
}

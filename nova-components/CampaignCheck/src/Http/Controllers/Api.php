<?php
namespace Twalton\CampaignCheck\Http\Controllers;
use App\CampaignCheck;

class Api
{
    /**
     * Return all the registered routes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getChecks($campaign)
    {
        //dd($campaign);
        //$languages = CampaignLang::where('campaign',$request->resourceId)->get();
        $checks = CampaignCheck::where('campaign','=',$campaign)->get();
        return response()->json($checks)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }


    public function deleteCheck($campaign,$stage,$sequence){        
        $check = CampaignCheck::where('campaign','=',$campaign)->where('stage','=',$stage)->where('sequence','=',$sequence)->delete();
        if($check) {
            return response()->json(['status' => 'success']);
        }
    }
    

    public function editCheck($campaign,$stage,$sequence,$validity_check)
    {                
        $record = CampaignCheck::where('campaign','=',$campaign)->where('stage','=',$stage)->where('sequence','=',$sequence)
        ->limit(1)
        ->update(['validity_check' => $validity_check ]);
        if($record) {
            return response()->json(['status' => 'success']);
        }
    }

    public function createCheck($campaign,$stage,$sequence,$validity_check)
    {          
        $check_new = new CampaignCheck;
        $check_new->campaign = $campaign;
        $check_new->validity_check = $validity_check;
        $check_new->sequence = $sequence;
        $check_new->stage = $stage;
        $check_new->save();
        if($check_new) {
            return response()->json(['status' => 'success']);
        }
    }
}
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. You're free to add
| as many additional routes to this file as your tool may require.
|
*/

Route::get('/{resoureceId}', 'Twalton\CampaignCheck\Http\Controllers\Api@getChecks')->name('depairs');
Route::delete('/{resoureceId}/{stage}/{sequence}', 'Twalton\CampaignCheck\Http\Controllers\Api@deleteCheck');
Route::get('/edit/{campaign}/{stage}/{sequence}/{validity}', 'Twalton\CampaignCheck\Http\Controllers\Api@editCheck');
Route::get('/create/{campaign}/{stage}/{sequence}/{validity}', 'Twalton\CampaignCheck\Http\Controllers\Api@createCheck');

<?php
namespace Twalton\ApplicationDestination\Http\Controllers;
use App\ApplicationDestination;

class Api
{
    /**
     * Return all the registered routes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDestinations($code)
    {
        //dd($code);
        //$languages = CampaignLang::where('code',$request->resourceId)->get();
        $dests = ApplicationDestination::where('code','=',$code)->get();
        return response()->json($dests)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function deleteDestination($code,$sequence){        
        $dest = ApplicationDestination::where('code','=',$code)->where('sequence','=',$sequence)->delete();
        if($dest) {
            return response()->json(['status' => 'success']);
        }
    }

    public function editDestination($code,$sequence,$destination)
    {                
        $record = ApplicationDestination::where('code','=',$code)->where('sequence','=',$sequence)
        ->limit(1)
        ->update([
            'destination' => $destination
        ]);
        if($record) {
            return response()->json(['status' => 'success']);
        }
    }

    public function createDestination($code,$sequence,$destination)
    {          
        dd($code);
        $dest_new = new ApplicationDestination;
        $dest_new->code = $code;
        $dest_new->destination = $destination;
        $dest_new->sequence = $sequence;
        $dest_new->save();
        if($dest_new) {
            return response()->json(['status' => 'success']);
        }
    }

}
<?php

namespace Twalton\ApplicationDestination;

use Laravel\Nova\ResourceTool;

class ApplicationDestination extends ResourceTool
{
    /**
     * Get the displayable name of the resource tool.
     *
     * @return string
     */
    public function name()
    {
        return 'Application Destination';
    }

    /**
     * Get the component name for the resource tool.
     *
     * @return string
     */
    public function component()
    {
        return 'application-destination';
    }
}

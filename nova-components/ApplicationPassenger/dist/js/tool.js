/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file.
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate

    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(2);
module.exports = __webpack_require__(9);


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

Nova.booting(function (Vue, router, store) {
  Vue.component('application-passenger', __webpack_require__(3));
});

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(4)
/* template */
var __vue_template__ = __webpack_require__(8)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/Tool.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-68ff5483", Component.options)
  } else {
    hotAPI.reload("data-v-68ff5483", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__PassengerTable__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__PassengerTable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__PassengerTable__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['resourceName', 'resourceId', 'field'],
    components: { PassengerTable: __WEBPACK_IMPORTED_MODULE_0__PassengerTable___default.a },
    data: function data() {
        return {
            passengers: [],
            search: '',
            createFlag: '',
            sort: {
                field: '',
                order: -1
            },
            form: {
                'code': '',
                'sequence': ''
            }
        };
    },
    mounted: function mounted() {
        this.getPassengers();
        this.form.code = this.resourceId;
    },

    methods: {
        getPassengers: function getPassengers() {
            var _this = this;

            Nova.request().get('/nova-vendor/application-passenger/' + this.resourceId).then(function (response) {
                console.log(JSON.stringify(response.data));
                _this.passengers = response.data;
            });
        },
        sortBy: function sortBy(field) {
            var _this2 = this;

            this.sort.field = field;
            this.sort.order *= -1;
            this.passengers.sort(function (passenger1, passenger2) {
                var comparison = 0;
                if (passenger1[_this2.sort.field] < passenger2[_this2.sort.field]) {
                    comparison = -1;
                }
                if (passenger1[_this2.sort.field] > passenger2[_this2.sort.field]) {
                    comparison = 1;
                }
                return comparison * _this2.sort.order;
            });
        },
        onCancel: function onCancel() {
            this.createFlag = '';
        },
        onCreateSubmit: function onCreateSubmit() {
            var _this3 = this;

            console.log(this.form.passport);
            var app = this; //doesn't like this but app it might
            Nova.request().post('/nova-vendor/application-passenger/create', this.form).then(function (response) {
                //do something                       
                _this3.passengers.push({
                    code: app.form.code,
                    sequence: app.form.sequence,
                    title: app.form.title,
                    first_name: app.form.first_name,
                    last_name: app.form.last_name,
                    dob: app.form.dob,
                    passport: app.form.passport,
                    passport_file: app.form.passport_file
                });
                _this3.createFlag = '';
            });
        },
        onCreate: function onCreate() {
            this.createFlag = 1;
        }
    },
    computed: {
        sortedPassengers: function sortedPassengers() {
            var _this4 = this;

            if (!this.search.length) {
                return this.passengers;
            }
            var regex = this.searchRegex;
            // User input is not a valid regular expression, show no results
            //console.log(regex);
            if (!regex) {
                return {};
            }

            return this.passengers.filter(function (passenger) {
                var matchesSearch = false;
                for (var key in passenger) {
                    if (Array.isArray(passenger[key])) {
                        passenger[key].forEach(function (property) {
                            if (regex.test(property)) {
                                matchesSearch = true;
                            }
                        });
                    } else if (regex.test(passenger[key])) {
                        matchesSearch = true;
                    }
                }
                return matchesSearch;
            });
            return this.passengers.sort(function (a, b) {
                var modifier = 1;
                if (_this4.currentSortDir === 'desc') modifier = -1;
                if (a[_this4.currentSort] < b[_this4.currentSort]) return -1 * modifier;
                if (a[_this4.currentSort] > b[_this4.currentSort]) return 1 * modifier;
                return 0;
            });
        },
        searchRegex: function searchRegex() {
            try {
                //console.log(this.search);
                return new RegExp('(' + this.search + ')', 'i');
            } catch (e) {
                return false;
            }
        }
    }
});

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(6)
/* template */
var __vue_template__ = __webpack_require__(7)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/components/PassengerTable.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-bbed2f02", Component.options)
  } else {
    hotAPI.reload("data-v-bbed2f02", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var StyleGenerator = function () {
    var styleIndex = 0;
    var usedStyles = {};
    var styles = [];
    return {
        generate: function generate(value) {
            if (styleIndex >= styles.length) {
                styleIndex = 0;
            }
            if (!usedStyles.hasOwnProperty(value)) {
                usedStyles[value] = styles[styleIndex];
            }
            styleIndex++;
            return usedStyles[value];
        }
    };
}();
/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            editId: '',
            deleteId: '',
            viewId: '',
            fields: [{
                label: 'Code',
                attribute: 'Code'
            }, {
                label: 'Sequence',
                attribute: 'sequence'
            }],
            editPassengerData: {
                'code': '',
                'sequence': ''
            },
            viewPassengerData: {
                'code': '',
                'sequence': ''
            }
        };
    },

    props: {
        passengers: {
            type: Array,
            required: true
        },
        sort: {
            type: Function
        }
    },

    methods: {
        style: function style(value) {
            return StyleGenerator.generate(value);
        },
        onDelete: function onDelete(code, passenger, index) {
            this.deleteId = index;
        },
        deleteEntry: function deleteEntry(code, passenger, index) {
            this.deleteId = '';
            this.editId = '';
            var app = this; //doesn't like this but app it might
            Nova.request().delete('/nova-vendor/application-passenger/' + code + '/' + passenger).then(function (resp) {
                app.passengers.splice(index, 1);
            });
        },
        onEdit: function onEdit(passenger, index) {
            this.editId = index;
            this.editPassengerData.code = passenger.code;
            this.editPassengerData.sequence = passenger.sequence;
        },
        onCancel: function onCancel() {
            this.editId = '';
            this.deleteId = '';
            this.viewId = '';
            this.editPassengerData.code = '';
            this.editPassengerData.sequence = '';
        },
        onView: function onView(passenger, index) {
            this.viewId = index;
            this.viewPassengerData.code = passenger.code;
            this.viewPassengerData.sequence = passenger.sequence;
        },
        onEditSubmit: function onEditSubmit(code, passenger) {
            var app = this; //doesn't like this but app it might
            Nova.request().get('/nova-vendor/application-passenger/edit/' + code + '/' + this.editPassengerData.sequence).then(function (resp) {});
            this.editId = '';
            this.deleteId = '';
            this.viewId = '';
        }
    }
});

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "overflow-hidden overflow-x-auto relative" },
    [
      _vm.viewId !== ""
        ? [
            _c("div", { staticClass: "card mb-6 py-3 px-6" }, [
              _c("div", { staticClass: "flex border-b border-40" }, [
                _vm._m(0),
                _vm._v(" "),
                _c("div", { staticClass: "w-3/4 py-4 break-words" }, [
                  _c("p", { staticClass: "text-90" }, [
                    _vm._v(_vm._s(_vm.passengers[_vm.viewId].code))
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "flex border-b border-40" }, [
                _vm._m(1),
                _vm._v(" "),
                _c("div", { staticClass: "w-3/4 py-4 break-words" }, [
                  _c("p", { staticClass: "text-90" }, [
                    _vm._v(_vm._s(_vm.passengers[_vm.viewId].sequence))
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "flex border-b border-40" }, [
                _vm._m(2),
                _vm._v(" "),
                _c("div", { staticClass: "w-3/4 py-4 break-words" }, [
                  _c("p", { staticClass: "text-90" }, [
                    _vm._v(_vm._s(_vm.passengers[_vm.viewId].title))
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "flex border-b border-40" }, [
                _vm._m(3),
                _vm._v(" "),
                _c("div", { staticClass: "w-3/4 py-4 break-words" }, [
                  _c("p", [
                    _vm._v(_vm._s(_vm.passengers[_vm.viewId].first_name))
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "flex border-b border-40" }, [
                _vm._m(4),
                _vm._v(" "),
                _c("div", { staticClass: "w-3/4 py-4 break-words" }, [
                  _c("p", { staticClass: "text-90" }, [
                    _vm._v(_vm._s(_vm.passengers[_vm.viewId].last_name))
                  ])
                ])
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "flex border-b border-40 remove-bottom-border" },
                [
                  _vm._m(5),
                  _vm._v(" "),
                  _c("div", { staticClass: "w-3/4 py-4 break-words" }, [
                    _c("p", [_vm._v(_vm._s(_vm.passengers[_vm.viewId].dob))])
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "flex border-b border-40 remove-bottom-border" },
                [
                  _vm._m(6),
                  _vm._v(" "),
                  _c("div", { staticClass: "w-3/4 py-4 break-words" }, [
                    _c("p", [
                      _vm._v(_vm._s(_vm.passengers[_vm.viewId].passport))
                    ])
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "flex border-b border-40 remove-bottom-border" },
                [
                  _vm._m(7),
                  _vm._v(" "),
                  _c("div", { staticClass: "w-3/4 py-4 break-words" }, [
                    _c("p", [
                      _vm._v(_vm._s(_vm.passengers[_vm.viewId].passport_file))
                    ])
                  ])
                ]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "flex mb-6 mr-6 items-center" }, [
              _c(
                "a",
                {
                  staticClass:
                    "btn btn-link dim cursor-pointer text-80 ml-auto mr-6",
                  attrs: { tabindex: "0" },
                  on: { click: _vm.onCancel }
                },
                [_vm._v("\n                Back\n            ")]
              )
            ])
          ]
        : _vm.editId !== ""
        ? [
            _c("div", { staticClass: "flex border-b border-40" }, [
              _vm._m(8),
              _vm._v(" "),
              _c("div", { staticClass: "py-6 px-8 w-1/2" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.passengers[_vm.editId].code,
                      expression: "passengers[editId].code"
                    }
                  ],
                  staticClass:
                    "w-full form-control form-input form-input-bordered",
                  attrs: {
                    id: "code",
                    type: "text",
                    disabled: "",
                    placeholder: "Application"
                  },
                  domProps: { value: _vm.passengers[_vm.editId].code },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(
                        _vm.passengers[_vm.editId],
                        "code",
                        $event.target.value
                      )
                    }
                  }
                }),
                _vm._v(" "),
                _c("div", { staticClass: "help-text help-text mt-2" })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "flex border-b border-40" }, [
              _vm._m(9),
              _vm._v(" "),
              _c("div", { staticClass: "py-6 px-8 w-1/2" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.passengers[_vm.editId].sequence,
                      expression: "passengers[editId].sequence"
                    }
                  ],
                  staticClass:
                    "w-full form-control form-input form-input-bordered",
                  attrs: {
                    id: "sequence",
                    type: "number",
                    disabled: "",
                    placeholder: "Sequence"
                  },
                  domProps: { value: _vm.passengers[_vm.editId].sequence },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(
                        _vm.passengers[_vm.editId],
                        "sequence",
                        $event.target.value
                      )
                    }
                  }
                }),
                _vm._v(" "),
                _c("div", { staticClass: "help-text help-text mt-2" })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "flex border-b border-40" }, [
              _vm._m(10),
              _vm._v(" "),
              _c("div", { staticClass: "py-6 px-8 w-1/2" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.passengers[_vm.editId].title,
                      expression: "passengers[editId].title"
                    }
                  ],
                  staticClass:
                    "w-full form-control form-input form-input-bordered",
                  attrs: {
                    id: "title",
                    type: "number",
                    min: "1",
                    max: "2",
                    placeholder: "Title"
                  },
                  domProps: { value: _vm.passengers[_vm.editId].title },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(
                        _vm.passengers[_vm.editId],
                        "title",
                        $event.target.value
                      )
                    }
                  }
                }),
                _vm._v(" "),
                _c("div", { staticClass: "help-text help-text mt-2" })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "flex border-b border-40" }, [
              _vm._m(11),
              _vm._v(" "),
              _c("div", { staticClass: "py-6 px-8 w-1/2" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.passengers[_vm.editId].first_name,
                      expression: "passengers[editId].first_name"
                    }
                  ],
                  staticClass:
                    "w-full form-control form-input form-input-bordered",
                  attrs: {
                    id: "first_name",
                    type: "text",
                    placeholder: "First Name"
                  },
                  domProps: { value: _vm.passengers[_vm.editId].first_name },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(
                        _vm.passengers[_vm.editId],
                        "first_name",
                        $event.target.value
                      )
                    }
                  }
                }),
                _vm._v(" "),
                _c("div", { staticClass: "help-text help-text mt-2" })
              ])
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "flex border-b border-40 remove-bottom-border" },
              [
                _vm._m(12),
                _vm._v(" "),
                _c("div", { staticClass: "py-6 px-8 w-1/2" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.passengers[_vm.editId].last_name,
                        expression: "passengers[editId].last_name"
                      }
                    ],
                    staticClass:
                      "w-full form-control form-input form-input-bordered",
                    attrs: {
                      id: "last_name",
                      type: "text",
                      placeholder: "Last_name"
                    },
                    domProps: { value: _vm.passengers[_vm.editId].last_name },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(
                          _vm.passengers[_vm.editId],
                          "last_name",
                          $event.target.value
                        )
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("div", { staticClass: "help-text help-text mt-2" })
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "flex border-b border-40 remove-bottom-border" },
              [
                _vm._m(13),
                _vm._v(" "),
                _c("div", { staticClass: "py-6 px-8 w-1/2" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.passengers[_vm.editId].dob,
                        expression: "passengers[editId].dob"
                      }
                    ],
                    staticClass:
                      "w-full form-control form-input form-input-bordered",
                    attrs: {
                      id: "dob",
                      type: "date",
                      placeholder: "Date of Birth"
                    },
                    domProps: { value: _vm.passengers[_vm.editId].dob },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(
                          _vm.passengers[_vm.editId],
                          "dob",
                          $event.target.value
                        )
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("div", { staticClass: "help-text help-text mt-2" })
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "flex border-b border-40 remove-bottom-border" },
              [
                _vm._m(14),
                _vm._v(" "),
                _c("div", { staticClass: "py-6 px-8 w-1/2" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.passengers[_vm.editId].passport,
                        expression: "passengers[editId].passport"
                      }
                    ],
                    staticClass:
                      "w-full form-control form-input form-input-bordered",
                    attrs: {
                      id: "passport",
                      type: "text",
                      placeholder: "Passport"
                    },
                    domProps: { value: _vm.passengers[_vm.editId].passport },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(
                          _vm.passengers[_vm.editId],
                          "passport",
                          $event.target.value
                        )
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("div", { staticClass: "help-text help-text mt-2" })
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "flex border-b border-40 remove-bottom-border" },
              [
                _vm._m(15),
                _vm._v(" "),
                _c("div", { staticClass: "py-6 px-8 w-1/2" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.passengers[_vm.editId].passport_file,
                        expression: "passengers[editId].passport_file"
                      }
                    ],
                    staticClass:
                      "w-full form-control form-input form-input-bordered",
                    attrs: {
                      id: "passport_file",
                      type: "text",
                      placeholder: "Passport File"
                    },
                    domProps: {
                      value: _vm.passengers[_vm.editId].passport_file
                    },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(
                          _vm.passengers[_vm.editId],
                          "passport_file",
                          $event.target.value
                        )
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("div", { staticClass: "help-text help-text mt-2" })
                ])
              ]
            ),
            _vm._v(" "),
            _c("div", { staticClass: "flex mb-6 mr-6 items-center" }, [
              _c(
                "a",
                {
                  staticClass:
                    "btn btn-link dim cursor-pointer text-80 ml-auto mr-6",
                  attrs: { tabindex: "0" },
                  on: { click: _vm.onCancel }
                },
                [_vm._v("\n                Cancel\n            ")]
              ),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass:
                    "btn btn-default btn-primary inline-flex items-center relative",
                  attrs: { type: "submit", dusk: "update-button" },
                  on: {
                    click: function($event) {
                      return _vm.onEditSubmit(
                        _vm.passengers[_vm.editId].code,
                        _vm.passenger
                      )
                    }
                  }
                },
                [
                  _c("span", {}, [
                    _vm._v(
                      "\n                    Update Application Passenger\n                "
                    )
                  ])
                ]
              )
            ])
          ]
        : _vm.deleteId !== ""
        ? [
            _c("div", { staticClass: "card mb-6 py-3 px-6" }, [
              _c("div", { staticClass: "flex border-b border-40" }, [
                _vm._m(16),
                _vm._v(" "),
                _c("div", { staticClass: "w-3/4 py-4 break-words" }, [
                  _c("p", { staticClass: "text-90" }, [
                    _vm._v(_vm._s(_vm.passengers[_vm.deleteId].code))
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "flex border-b border-40" }, [
                _vm._m(17),
                _vm._v(" "),
                _c("div", { staticClass: "w-3/4 py-4 break-words" }, [
                  _c("p", { staticClass: "text-90" }, [
                    _vm._v(_vm._s(_vm.passengers[_vm.deleteId].sequence))
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "flex border-b border-40" }, [
                _vm._m(18),
                _vm._v(" "),
                _c("div", { staticClass: "w-3/4 py-4 break-words" }, [
                  _c("p", { staticClass: "text-90" }, [
                    _vm._v(_vm._s(_vm.passengers[_vm.deleteId].title))
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "flex border-b border-40" }, [
                _vm._m(19),
                _vm._v(" "),
                _c("div", { staticClass: "w-3/4 py-4 break-words" }, [
                  _c("p", [
                    _vm._v(_vm._s(_vm.passengers[_vm.deleteId].first_name))
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "flex border-b border-40" }, [
                _vm._m(20),
                _vm._v(" "),
                _c("div", { staticClass: "w-3/4 py-4 break-words" }, [
                  _c("p", { staticClass: "text-90" }, [
                    _vm._v(_vm._s(_vm.passengers[_vm.deleteId].last_name))
                  ])
                ])
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "flex border-b border-40 remove-bottom-border" },
                [
                  _vm._m(21),
                  _vm._v(" "),
                  _c("div", { staticClass: "w-3/4 py-4 break-words" }, [
                    _c("p", [_vm._v(_vm._s(_vm.passengers[_vm.deleteId].dob))])
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "flex border-b border-40 remove-bottom-border" },
                [
                  _vm._m(22),
                  _vm._v(" "),
                  _c("div", { staticClass: "w-3/4 py-4 break-words" }, [
                    _c("p", [
                      _vm._v(_vm._s(_vm.passengers[_vm.deleteId].passport))
                    ])
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "flex border-b border-40 remove-bottom-border" },
                [
                  _vm._m(23),
                  _vm._v(" "),
                  _c("div", { staticClass: "w-3/4 py-4 break-words" }, [
                    _c("p", [
                      _vm._v(_vm._s(_vm.passengers[_vm.deleteId].passport_file))
                    ])
                  ])
                ]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "flex mb-6 mr-6 items-center" }, [
              _c(
                "a",
                {
                  staticClass:
                    "btn btn-link dim cursor-pointer text-80 ml-auto mr-6",
                  attrs: { tabindex: "0" },
                  on: { click: _vm.onCancel }
                },
                [_vm._v("\n                Cancel\n            ")]
              ),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass:
                    "btn btn-default btn-primary inline-flex items-center relative",
                  attrs: { type: "submit", dusk: "update-button" },
                  on: {
                    click: function($event) {
                      return _vm.deleteEntry(
                        _vm.passengers[_vm.deleteId].code,
                        _vm.passengers[_vm.deleteId].sequence,
                        _vm.deleteId
                      )
                    }
                  }
                },
                [
                  _c("span", {}, [
                    _vm._v("\n                    Delete\n                ")
                  ])
                ]
              )
            ])
          ]
        : [
            _c(
              "table",
              {
                staticClass: "table w-full",
                attrs: { cellpadding: "0", cellspacing: "0" }
              },
              [
                _c("thead", [
                  _c(
                    "tr",
                    [
                      _vm._l(_vm.fields, function(field, index) {
                        return _c(
                          "th",
                          { key: index, staticClass: "text-left" },
                          [
                            _c(
                              "sortable-icon",
                              {
                                attrs: {
                                  "resource-name": _vm.resourceName,
                                  "uri-key": field.attribute
                                },
                                on: {
                                  sort: function($event) {
                                    return _vm.sort(field.attribute)
                                  }
                                }
                              },
                              [
                                _vm._v(
                                  "\n                        " +
                                    _vm._s(_vm.__(field.label)) +
                                    "\n                    "
                                )
                              ]
                            )
                          ],
                          1
                        )
                      }),
                      _vm._v(" "),
                      _c("th", [_vm._v(" ")])
                    ],
                    2
                  )
                ]),
                _vm._v(" "),
                _c(
                  "tbody",
                  _vm._l(_vm.passengers, function(passenger, index) {
                    return _c(
                      "tr",
                      { key: index, attrs: { passenger: passenger } },
                      [
                        _c(
                          "td",
                          { staticClass: "whitespace-no-wrap text-left" },
                          [
                            _vm._v(
                              "\n                        " +
                                _vm._s(passenger.code) +
                                "\n                    "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "td",
                          { staticClass: "whitespace-no-wrap text-left" },
                          [
                            _vm._v(
                              "\n                        " +
                                _vm._s(passenger.sequence) +
                                "\n                    "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("td", { staticClass: "td-fit text-right pr-6" }, [
                          _c(
                            "button",
                            {
                              staticClass:
                                "cursor-pointer text-70 hover:text-primary mr-3",
                              on: {
                                click: function($event) {
                                  return _vm.onView(passenger, index)
                                }
                              }
                            },
                            [
                              _c(
                                "svg",
                                {
                                  staticClass: "fill-current",
                                  attrs: {
                                    xmlns: "http://www.w3.org/2000/svg",
                                    width: "22",
                                    height: "18",
                                    viewBox: "0 0 22 16",
                                    "aria-labelledby": "view",
                                    role: "presentation"
                                  }
                                },
                                [
                                  _c("path", {
                                    attrs: {
                                      d:
                                        "M16.56 13.66a8 8 0 0 1-11.32 0L.3 8.7a1 1 0 0 1 0-1.42l4.95-4.95a8 8 0 0 1 11.32 0l4.95 4.95a1 1 0 0 1 0 1.42l-4.95 4.95-.01.01zm-9.9-1.42a6 6 0 0 0 8.48 0L19.38 8l-4.24-4.24a6 6 0 0 0-8.48 0L2.4 8l4.25 4.24h.01zM10.9 12a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm0-2a2 2 0 1 0 0-4 2 2 0 0 0 0 4z"
                                    }
                                  })
                                ]
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "button",
                            {
                              staticClass:
                                "cursor-pointer text-70 hover:text-primary mr-3",
                              on: {
                                click: function($event) {
                                  return _vm.onEdit(passenger, index)
                                }
                              }
                            },
                            [
                              _c(
                                "svg",
                                {
                                  staticClass: "fill-current",
                                  attrs: {
                                    xmlns: "http://www.w3.org/2000/svg",
                                    width: "20",
                                    height: "20",
                                    viewBox: "0 0 20 20",
                                    "aria-labelledby": "edit",
                                    role: "presentation"
                                  }
                                },
                                [
                                  _c("path", {
                                    attrs: {
                                      d:
                                        "M4.3 10.3l10-10a1 1 0 0 1 1.4 0l4 4a1 1 0 0 1 0 1.4l-10 10a1 1 0 0 1-.7.3H5a1 1 0 0 1-1-1v-4a1 1 0 0 1 .3-.7zM6 14h2.59l9-9L15 2.41l-9 9V14zm10-2a1 1 0 0 1 2 0v6a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4c0-1.1.9-2 2-2h6a1 1 0 1 1 0 2H2v14h14v-6z"
                                    }
                                  })
                                ]
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "button",
                            {
                              staticClass:
                                "cursor-pointer text-70 hover:text-primary mr-3",
                              on: {
                                click: function($event) {
                                  return _vm.onDelete(
                                    passenger.code,
                                    passenger.sequence,
                                    index
                                  )
                                }
                              }
                            },
                            [
                              _c(
                                "svg",
                                {
                                  staticClass: "fill-current",
                                  attrs: {
                                    xmlns: "http://www.w3.org/2000/svg",
                                    width: "20",
                                    height: "20",
                                    viewBox: "0 0 20 20",
                                    "aria-labelledby": "delete",
                                    role: "presentation"
                                  }
                                },
                                [
                                  _c("path", {
                                    attrs: {
                                      "fill-rule": "nonzero",
                                      d:
                                        "M6 4V2a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2h5a1 1 0 0 1 0 2h-1v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6H1a1 1 0 1 1 0-2h5zM4 6v12h12V6H4zm8-2V2H8v2h4zM8 8a1 1 0 0 1 1 1v6a1 1 0 0 1-2 0V9a1 1 0 0 1 1-1zm4 0a1 1 0 0 1 1 1v6a1 1 0 0 1-2 0V9a1 1 0 0 1 1-1z"
                                    }
                                  })
                                ]
                              )
                            ]
                          )
                        ])
                      ]
                    )
                  }),
                  0
                )
              ]
            )
          ]
    ],
    2
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/4 py-4" }, [
      _c("h4", { staticClass: "font-normal text-80" }, [_vm._v("Application")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/4 py-4" }, [
      _c("h4", { staticClass: "font-normal text-80" }, [_vm._v("Sequence")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/4 py-4" }, [
      _c("h4", { staticClass: "font-normal text-80" }, [_vm._v("Title")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/4 py-4" }, [
      _c("h4", { staticClass: "font-normal text-80" }, [_vm._v("First Name")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/4 py-4" }, [
      _c("h4", { staticClass: "font-normal text-80" }, [_vm._v("Last Name")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/4 py-4" }, [
      _c("h4", { staticClass: "font-normal text-80" }, [
        _vm._v("Date of Birth")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/4 py-4" }, [
      _c("h4", { staticClass: "font-normal text-80" }, [_vm._v("Passport")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/4 py-4" }, [
      _c("h4", { staticClass: "font-normal text-80" }, [
        _vm._v("Passport File")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/5 px-8 py-6" }, [
      _c(
        "label",
        {
          staticClass: "inline-block text-80 pt-2 leading-tight",
          attrs: { for: "code" }
        },
        [_vm._v("Application")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/5 px-8 py-6" }, [
      _c(
        "label",
        {
          staticClass: "inline-block text-80 pt-2 leading-tight",
          attrs: { for: "type" }
        },
        [_vm._v("Sequence")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/5 px-8 py-6" }, [
      _c(
        "label",
        {
          staticClass: "inline-block text-80 pt-2 leading-tight",
          attrs: { for: "title" }
        },
        [_vm._v("Title")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/5 px-8 py-6" }, [
      _c(
        "label",
        {
          staticClass: "inline-block text-80 pt-2 leading-tight",
          attrs: { for: "first_name" }
        },
        [_vm._v("First Name")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/5 px-8 py-6" }, [
      _c(
        "label",
        {
          staticClass: "inline-block text-80 pt-2 leading-tight",
          attrs: { for: "last_name" }
        },
        [_vm._v("Last Name")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/5 px-8 py-6" }, [
      _c(
        "label",
        {
          staticClass: "inline-block text-80 pt-2 leading-tight",
          attrs: { for: "dob" }
        },
        [_vm._v("Date of Birth")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/5 px-8 py-6" }, [
      _c(
        "label",
        {
          staticClass: "inline-block text-80 pt-2 leading-tight",
          attrs: { for: "passport" }
        },
        [_vm._v("Passport")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/5 px-8 py-6" }, [
      _c(
        "label",
        {
          staticClass: "inline-block text-80 pt-2 leading-tight",
          attrs: { for: "passport_file" }
        },
        [_vm._v("Passport File")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/4 py-4" }, [
      _c("h4", { staticClass: "font-normal text-80" }, [_vm._v("Application")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/4 py-4" }, [
      _c("h4", { staticClass: "font-normal text-80" }, [_vm._v("Sequence")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/4 py-4" }, [
      _c("h4", { staticClass: "font-normal text-80" }, [_vm._v("Title")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/4 py-4" }, [
      _c("h4", { staticClass: "font-normal text-80" }, [_vm._v("First Name")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/4 py-4" }, [
      _c("h4", { staticClass: "font-normal text-80" }, [_vm._v("Last_name")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/4 py-4" }, [
      _c("h4", { staticClass: "font-normal text-80" }, [
        _vm._v("Date of Birth")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/4 py-4" }, [
      _c("h4", { staticClass: "font-normal text-80" }, [_vm._v("Passport")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "w-1/4 py-4" }, [
      _c("h4", { staticClass: "font-normal text-80" }, [
        _vm._v("Passport File")
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-bbed2f02", module.exports)
  }
}

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("heading", { staticClass: "mb-6" }, [
        _vm._v("Application Passengers")
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "relative h-9 flex items-center mb-6" },
        [
          _c("icon", {
            staticClass: "absolute ml-3 text-70",
            attrs: { type: "search" }
          }),
          _vm._v(" "),
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.search,
                expression: "search"
              }
            ],
            staticClass:
              "appearance-none form-search w-search pl-search shadow",
            attrs: { placeholder: _vm.__("Search"), type: "search" },
            domProps: { value: _vm.search },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.search = $event.target.value
              }
            }
          }),
          _vm._v(" "),
          _c("div", { staticClass: "ml-3 w-full flex items-center" }, [
            _c("div", { staticClass: "flex w-full justify-end items-center" }),
            _vm._v(" "),
            _vm._m(0),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass:
                  "btn btn-default btn-icon btn-primary inline-flex items-center relative",
                attrs: { type: "submit" },
                on: {
                  click: function($event) {
                    return _vm.onCreate()
                  }
                }
              },
              [_vm._v("Create")]
            )
          ])
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "card",
        [
          _vm.createFlag !== ""
            ? [
                _c(
                  "form",
                  {
                    on: {
                      submit: function($event) {
                        $event.preventDefault()
                      }
                    }
                  },
                  [
                    _c("div", { staticClass: "flex border-b border-40" }, [
                      _c("div", { staticClass: "w-1/5 px-8 py-6" }, [
                        _c(
                          "label",
                          {
                            staticClass:
                              "inline-block text-80 pt-2 leading-tight",
                            attrs: { for: "code" }
                          },
                          [_vm._v("Application")]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "py-6 px-8 w-1/2" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.form.code,
                              expression: "form.code"
                            }
                          ],
                          staticClass:
                            "w-full form-control form-input form-input-bordered",
                          attrs: {
                            disabled: "",
                            id: "code",
                            type: "text",
                            placeholder: "Application"
                          },
                          domProps: { value: _vm.form.code },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(_vm.form, "code", $event.target.value)
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("div", { staticClass: "help-text help-text mt-2" })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "flex border-b border-40" }, [
                      _c("div", { staticClass: "w-1/5 px-8 py-6" }, [
                        _c(
                          "label",
                          {
                            staticClass:
                              "inline-block text-80 pt-2 leading-tight",
                            attrs: { for: "sequence" }
                          },
                          [_vm._v("Sequence")]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "py-6 px-8 w-1/2" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.form.sequence,
                              expression: "form.sequence"
                            }
                          ],
                          staticClass:
                            "w-full form-control form-input form-input-bordered",
                          attrs: {
                            id: "sequence",
                            type: "number",
                            min: "0",
                            placeholder: "Sequence"
                          },
                          domProps: { value: _vm.form.sequence },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.form,
                                "sequence",
                                $event.target.value
                              )
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("div", { staticClass: "help-text help-text mt-2" })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "flex border-b border-40" }, [
                      _c("div", { staticClass: "w-1/5 px-8 py-6" }, [
                        _c(
                          "label",
                          {
                            staticClass:
                              "inline-block text-80 pt-2 leading-tight",
                            attrs: { for: "title" }
                          },
                          [_vm._v("Title")]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "py-6 px-8 w-1/2" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.form.title,
                              expression: "form.title"
                            }
                          ],
                          staticClass:
                            "w-full form-control form-input form-input-bordered",
                          attrs: {
                            id: "title",
                            type: "number",
                            min: "1",
                            max: "2",
                            placeholder: "Title"
                          },
                          domProps: { value: _vm.form.title },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(_vm.form, "title", $event.target.value)
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("div", { staticClass: "help-text help-text mt-2" })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "flex border-b border-40" }, [
                      _c("div", { staticClass: "w-1/5 px-8 py-6" }, [
                        _c(
                          "label",
                          {
                            staticClass:
                              "inline-block text-80 pt-2 leading-tight",
                            attrs: { for: "first_name" }
                          },
                          [_vm._v("First Name")]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "py-6 px-8 w-1/2" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.form.first_name,
                              expression: "form.first_name"
                            }
                          ],
                          staticClass:
                            "w-full form-control form-input form-input-bordered",
                          attrs: {
                            id: "first_name",
                            type: "text",
                            placeholder: "First Name"
                          },
                          domProps: { value: _vm.form.first_name },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.form,
                                "first_name",
                                $event.target.value
                              )
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("div", { staticClass: "help-text help-text mt-2" })
                      ])
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass:
                          "flex border-b border-40 remove-bottom-border"
                      },
                      [
                        _c("div", { staticClass: "w-1/5 px-8 py-6" }, [
                          _c(
                            "label",
                            {
                              staticClass:
                                "inline-block text-80 pt-2 leading-tight",
                              attrs: { for: "last_name" }
                            },
                            [_vm._v("Last Name")]
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "py-6 px-8 w-1/2" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.last_name,
                                expression: "form.last_name"
                              }
                            ],
                            staticClass:
                              "w-full form-control form-input form-input-bordered",
                            attrs: {
                              id: "last_name",
                              type: "text",
                              placeholder: "Last Name"
                            },
                            domProps: { value: _vm.form.last_name },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "last_name",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("div", { staticClass: "help-text help-text mt-2" })
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass:
                          "flex border-b border-40 remove-bottom-border"
                      },
                      [
                        _c("div", { staticClass: "w-1/5 px-8 py-6" }, [
                          _c(
                            "label",
                            {
                              staticClass:
                                "inline-block text-80 pt-2 leading-tight",
                              attrs: { for: "dob" }
                            },
                            [_vm._v("Date of Birth")]
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "py-6 px-8 w-1/2" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.dob,
                                expression: "form.dob"
                              }
                            ],
                            staticClass:
                              "w-full form-control form-input form-input-bordered",
                            attrs: {
                              id: "dob",
                              type: "date",
                              placeholder: "Date of Birth"
                            },
                            domProps: { value: _vm.form.dob },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(_vm.form, "dob", $event.target.value)
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("div", { staticClass: "help-text help-text mt-2" })
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass:
                          "flex border-b border-40 remove-bottom-border"
                      },
                      [
                        _c("div", { staticClass: "w-1/5 px-8 py-6" }, [
                          _c(
                            "label",
                            {
                              staticClass:
                                "inline-block text-80 pt-2 leading-tight",
                              attrs: { for: "passport" }
                            },
                            [_vm._v("Passport")]
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "py-6 px-8 w-1/2" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.passport,
                                expression: "form.passport"
                              }
                            ],
                            staticClass:
                              "w-full form-control form-input form-input-bordered",
                            attrs: {
                              id: "passport",
                              type: "text",
                              placeholder: "Passport"
                            },
                            domProps: { value: _vm.form.passport },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "passport",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("div", { staticClass: "help-text help-text mt-2" })
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass:
                          "flex border-b border-40 remove-bottom-border"
                      },
                      [
                        _c("div", { staticClass: "w-1/5 px-8 py-6" }, [
                          _c(
                            "label",
                            {
                              staticClass:
                                "inline-block text-80 pt-2 leading-tight",
                              attrs: { for: "passport_file" }
                            },
                            [_vm._v("Passport File")]
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "py-6 px-8 w-1/2" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.passport_file,
                                expression: "form.passport_file"
                              }
                            ],
                            staticClass:
                              "w-full form-control form-input form-input-bordered",
                            attrs: {
                              id: "passport_file",
                              type: "text",
                              placeholder: "Passport File"
                            },
                            domProps: { value: _vm.form.passport_file },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "passport_file",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("div", { staticClass: "help-text help-text mt-2" })
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "flex mb-6 mr-6 items-center" }, [
                      _c(
                        "a",
                        {
                          staticClass:
                            "btn btn-link dim cursor-pointer text-80 ml-auto mr-6",
                          attrs: { tabindex: "0" },
                          on: { click: _vm.onCancel }
                        },
                        [
                          _vm._v(
                            "\n                        Cancel\n                    "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass:
                            "btn btn-default btn-primary inline-flex items-center relative",
                          attrs: { type: "submit", dusk: "update-button" },
                          on: {
                            click: function($event) {
                              return _vm.onCreateSubmit()
                            }
                          }
                        },
                        [
                          _c("span", {}, [
                            _vm._v(
                              "\n                            Create Application Passenger\n                        "
                            )
                          ])
                        ]
                      )
                    ])
                  ]
                )
              ]
            : _c("passenger-table", {
                attrs: { passengers: _vm.sortedPassengers, sort: _vm.sortBy }
              })
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "ml-3" }, [
      _c("div", {
        staticClass: "v-portal",
        staticStyle: { display: "none" },
        attrs: { transition: "fade-transition" }
      })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-68ff5483", module.exports)
  }
}

/***/ }),
/* 9 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);
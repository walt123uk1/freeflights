<?php
namespace Twalton\ApplicationPassenger\Http\Controllers;
use App\ApplicationPassenger;
use Illuminate\Support\Str;
use Laravel\Nova\Http\Requests\NovaRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Log;


class Api
{
    /**
     * Return all the registered routes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPassengers($code)
    {
        //dd($code);
        $passenger = ApplicationPassenger::where('code','=',$code)->get();
        Log::debug($passenger);
        return response()->json($passenger)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }

    public function deletePassenger($code,$sequence){        
        $sequence = ApplicationPassenger::where('code','=',$code)->where('sequence','=',$sequence)->delete();
        if($sequence) {
            return response()->json(['status' => 'success']);
        }
    }

    public function editPassenger($code,$sequence)
    {                
        $record = ApplicationPassenger::where([['code','=',$code],['sequence','=',$sequence]])
        ->limit(1);
        // ->update(['alternate' => $alternate, 
        //'sequence'=> $sequence
        //]);
        if($record) {
            return response()->json(['status' => 'success']);
        }
    }

    public function createPassenger(Request $request)
    {       
        $passenger_application_new = new ApplicationPassenger;
        $passenger_application_new->code = $request->code;
        $passenger_application_new->sequence = $request->sequence;
        $passenger_application_new->title = $request->title;
        $passenger_application_new->first_name = $request->first_name;
        $passenger_application_new->last_name = $request->last_name;
        $passenger_application_new->dob = $request->dob;
        $passenger_application_new->passport = $request->passport;
        $passenger_application_new->passport_file = $request->passport_file;
        $passenger_application_new->save();
        if($passenger_application_new) {
            return response()->json(['status' => 'success']);
        }
    }
}
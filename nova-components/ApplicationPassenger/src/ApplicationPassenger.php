<?php

namespace Twalton\ApplicationPassenger;

use Laravel\Nova\ResourceTool;

class ApplicationPassenger extends ResourceTool
{
    /**
     * Get the displayable name of the resource tool.
     *
     * @return string
     */
    public function name()
    {
        return 'Application Passenger';
    }

    /**
     * Get the component name for the resource tool.
     *
     * @return string
     */
    public function component()
    {
        return 'application-passenger';
    }
}

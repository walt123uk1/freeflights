<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. You're free to add
| as many additional routes to this file as your tool may require.
|
*/

Route::get('/{resoureceId}', 'Twalton\ApplicationPassenger\Http\Controllers\Api@getPassengers');
Route::delete('/{resoureceId}/{sequence}', 'Twalton\ApplicationPassenger\Http\Controllers\Api@deletePassenger');
Route::get('/edit/{code}/{sequence}', 'Twalton\ApplicationPassenger\Http\Controllers\Api@editPassenger');
Route::post('/create', 'Twalton\ApplicationPassenger\Http\Controllers\Api@createPassenger');

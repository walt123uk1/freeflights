<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. You're free to add
| as many additional routes to this file as your tool may require.
|
*/

// Route::get('/endpoint', function (Request $request) {
//     //
// });


Route::get('/{resoureceId}', 'Twalton\CampaignDepartureAirport\Http\Controllers\Api@getDepairs')->name('depairs');
Route::delete('/{resoureceId}/{sequence}', 'Twalton\CampaignDepartureAirport\Http\Controllers\Api@deleteDepair');
Route::get('/edit/{campaign}/{sequence}/{iata}', 'Twalton\CampaignDepartureAirport\Http\Controllers\Api@editDepair');
Route::get('/create/{campaign}/{sequence}/{iata}', 'Twalton\CampaignDepartureAirport\Http\Controllers\Api@createDepair');

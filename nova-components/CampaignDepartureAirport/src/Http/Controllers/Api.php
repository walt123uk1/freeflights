<?php
namespace Twalton\CampaignDepartureAirport\Http\Controllers;
use App\CampaignDepartAirport;
use Illuminate\Support\Str;
use Laravel\Nova\Http\Requests\NovaRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as FacadesRequest;

class Api
{
    /**
     * Return all the registered routes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDepairs($campaign)
    {
        //dd($campaign);
        //$languages = CampaignLang::where('campaign',$request->resourceId)->get();
        $depairs = CampaignDepartAirport::where('campaign','=',$campaign)->get();
        return response()->json($depairs)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }


    public function deleteDepair($campaign,$sequence){        
        $depair = CampaignDepartAirport::where('campaign','=',$campaign)->where('sequence','=',$sequence)->delete();
        if($depair) {
            return response()->json(['status' => 'success']);
        }
    }
    

    public function editDepair($campaign,$sequence,$iata)
    {                
        $record = CampaignDepartAirport::where([['campaign','=',$campaign],['sequence','=',$sequence]])
        ->limit(1)
        ->update(['iata' => $iata ]);
        if($record) {
            return response()->json(['status' => 'success']);
        }
    }

    public function createDepair($campaign,$sequence,$iata)
    {          
        $depair_new = new CampaignDepartAirport;
        $depair_new->campaign = $campaign;
        $depair_new->iata = $iata;
        $depair_new->sequence = $sequence;
        $depair_new->save();
        if($depair_new) {
            return response()->json(['status' => 'success']);
        }
    }
}
<?php

namespace Twalton\CampaignDepartureAirport;

use Laravel\Nova\ResourceTool;

class CampaignDepartureAirport extends ResourceTool
{
    /**
     * Get the displayable name of the resource tool.
     *
     * @return string
     */
    public function name()
    {
        return 'Campaign Departure Airport';
    }

    /**
     * Get the component name for the resource tool.
     *
     * @return string
     */
    public function component()
    {
        return 'campaign-departure-airport';
    }
}

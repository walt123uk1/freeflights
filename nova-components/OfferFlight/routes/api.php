<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. You're free to add
| as many additional routes to this file as your tool may require.
|
*/

Route::get('/{resoureceId}', 'Twalton\OfferFlight\Http\Controllers\Api@getFlights')->name('languages');
Route::delete('/{resoureceId}/{sequence}', 'Twalton\OfferFlight\Http\Controllers\Api@deleteFlight');
Route::get('/edit/{offer}/{sequence}', 'Twalton\OfferFlight\Http\Controllers\Api@editFlight');
Route::post('/create', 'Twalton\OfferFlight\Http\Controllers\Api@createFlight');

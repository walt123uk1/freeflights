<?php
namespace Twalton\OfferFlight\Http\Controllers;
use App\OfferFlight;
use Illuminate\Support\Str;
use Laravel\Nova\Http\Requests\NovaRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as FacadesRequest;

class Api
{
    /**
     * Return all the registered routes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFlights($offer)
    {
        //dd($offer);
        //$sequences = OfferFlight::where('offer',$request->resourceId)->get();
        $flights = OfferFlight::where('offer','=',$offer)->get();
        return response()->json($flights)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }


    public function deleteFlight($offer,$sequence){        
        $sequence = OfferFlight::where('offer','=',$offer)->where('sequence','=',$sequence)->delete();
        if($sequence) {
            return response()->json(['status' => 'success']);
        }
    }
    

    public function editFlight($offer,$sequence)
    {                
        $record = OfferFlight::where([['offer','=',$offer],['sequence','=',$sequence]])
        ->limit(1);
        // ->update(['alternate' => $alternate, 
        //'sequence'=> $sequence
        //]);
        if($record) {
            return response()->json(['status' => 'success']);
        }
    }

    public function createFlight(Request $request)
    {       
        $flight_offer_new = new OfferFlight;
        $flight_offer_new->offer = $request->offer;
        $flight_offer_new->sequence = $request->sequence;
        $flight_offer_new->direction = $request->direction;
        $flight_offer_new->flight_id = $request->flight_id;
        $flight_offer_new->airline = $request->airline;
        $flight_offer_new->operator = $request->operator;
        $flight_offer_new->class = $request->class;
        $flight_offer_new->departure_date = $request->departure_date;
        $flight_offer_new->departure_airport = $request->departure_airport;
        $flight_offer_new->arrival_date = $request->arrival_date;
        $flight_offer_new->arrival_airport = $request->arrival_airport;
        $flight_offer_new->save();
        if($flight_offer_new) {
            return response()->json(['status' => 'success']);
        }
    }
}
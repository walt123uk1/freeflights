<?php

namespace Twalton\OfferFlight;

use Laravel\Nova\ResourceTool;

class OfferFlight extends ResourceTool
{
    /**
     * Get the displayable name of the resource tool.
     *
     * @return string
     */
    public function name()
    {
        return 'Offer Flight';
    }

    /**
     * Get the component name for the resource tool.
     *
     * @return string
     */
    public function component()
    {
        return 'offer-flight';
    }
}

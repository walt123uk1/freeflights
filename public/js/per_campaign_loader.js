/**
 * Load, if it's present, the current campaign's custom JS file
 * NOTE that this file gets the "current campaign" by looking in <meta name="forge-campaign-prefix">
 *
 * @author Daniel Rhodes <daniel.rhodes@inspireemail.co.uk>
 * 
 */

//note we could simply defer="defer" the loading of *this* script in the parent HTML, but older IEs
//do not support that apparently
$( document ).ready(function() {
    
    var campaignPrefix = document.querySelector('meta[name="forge-campaign-prefix"]').getAttribute('content');

    //the old school way (which works but will error in the debug console if no such .js file on server)
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = "/system/js/per_campaign/" + campaignPrefix + ".js";
    document.body.appendChild(script);
    
    
    //the jQuery way [https://api.jquery.com/jquery.getscript/] where we can somewhat
    //account for errors
    //$.getScript("/system/js/per_campaign/" + campaignPrefix + ".js");
    
});


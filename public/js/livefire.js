//gen4 javascript
var livefire_debug = false;
var livefire_lightbox = false;
var livefire_livetimer;
var livefire_livetarget ={};

$(document).ready(function() {
	
	/*if(!/mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit/.test(navigator.userAgent.toLowerCase())){
		$("select").select_skin();	
	}*/
	
	
	if(/MSIE /.test(navigator.userAgent.toLowerCase())){
		$("select").select_skin();	
	}
	
	$(function() {
		$( ".date :input" ).datepicker();
	});

	/*$('[oncontextmenu^="lf"]').click(function(e) {
    	e.preventDefault();
	});*/
		
	$('a[href$="#void"]').click(function(e) {
    	e.preventDefault();
	});
	
	if(start_instructions != false){
		//alert ('We have start instructions');
		//alert(start_instructions);
		livefire_parse_instructions(start_instructions);		
	}
	if(livefire_debug){
		$( "input[type=file]" ).change(
			
			function(event) {	
				alert( "Handler for .change() called. "+$(this).attr('name') );	
			
			}
			
		);
	}

	
});


/*
function livefire_liveform(){
$(".livefireform").submit(function(e)
{
 
    var formObj = $(this);
    var formURL = formObj.attr("action");
    var formData = new FormData(this);
    $.ajax({
        url: formURL,
		type: 'POST',
        data:  formData,
		mimeType:"multipart/form-data",
		contentType: false,
        cache: false,
        processData:false,
		success: function(data, textStatus, jqXHR)
    {
		alert("hey, i submitted");
    },
     error: function(jqXHR, textStatus, errorThrown) 
     {
     	alert("error");
     }          
    });
    e.preventDefault(); //Prevent Default action. 
    e.unbind();
}); 
}//*/


	
function lf(instruction, variable){	
	instruction = 'instruction='+instruction;

	if(instruction == 'instruction=goform'){
		//this should be removed once we upgrade the copy editor to a tiny mce replacement
		if(variable['usemce']){
			tinyMCE.triggerSave();
		}
		
		instruction+='&go-form-name='+variable['form']+'&'+$("#"+variable['form']).serialize()+'&form-name=livefire';
		
		if(variable['clicked']){
			instruction+='&'+variable['form']+ '_form-submitted='+variable['clicked'];
		}
	}else if(instruction == 'instruction=livedata'){
		instruction+='&field='+variable['target']+'&value='+$('#'+variable['target']).val();
		
	}else if(instruction == 'instruction=liveinput'){	
		livefire_livetarget[variable['target']]=true;
		
		clearTimeout( livefire_livetimer );
		
		livefire_livetimer=window.setTimeout( 
			function() {

				for(var key in livefire_livetarget)
				{
					delete livefire_livetarget[key];
					lf("livedata", {'target':key});
					
				}
				livefire_livetarget = [];
				clearTimeout( livefire_livetimer );
			}, 600
		);			
				
		return;
	}else{	

		for(key in variable){
			instruction+="&"+key+"="+variable[key];
		}
	}

	if(livefire_debug==2){
		alert(instruction);	
	}	
				
	$.ajax({
			type: 'POST',    
			url:'/livefire/',
			data:'form-name=livefire&livefire=1&'+instruction,
			success: function(result){
				if(livefire_debug){
					alert(result);	
				}		
				if(result.trim() !=''){
					instructions = $.parseJSON(result);
					livefire_parse_instructions(instructions);
				}
			}
	});

	event.preventDefault();//we've moved this to the back as it was preventing firefox from submitting forms //but it seems that it allows context menu's when editing */

}


function livefire_parse_instructions(instructions){
	for(key in instructions['instructions']){
		currentinstruction = instructions['instructions'][key];
		switch(currentinstruction['instruction']) {
			case 'livefire_debug':
				livefire_debug = currentinstruction['debug'];
				$( "body" ).append('<p class="sys_report">'+livefire_rendertime+' seconds / '+currentinstruction['count']+' DB Queries<br>Versions: '+currentinstruction['sysversion']+' (site: '+currentinstruction['siteversion']+')</p>');					
													
			    break;
			case 'livefire_redirect':
				window.location.href = currentinstruction['page'];
			    break;
			case 'livefire_reload':
				location.reload();
				break;
			case 'livefire_alert':
				alert(currentinstruction['message']);
				break;
			case 'livefire_popover':
				if(!livefire_lightbox){
					$( "body" ).append( '<div id="popover"></div><div id="lightbox"><div id="lightbox-shade" onclick="livefire_killlightbox()"></div><div id="lightbox-highlight-shade" onclick="livefire_killlightbox()"><div id="lightbox-shade-l"></div><div id="lightbox-shade-r"></div><div id="lightbox-shade-t"></div><div id="lightbox-shade-b"></div></div></div></div>');
					livefire_lightbox = true;
				}
				
				$("#popover").html(currentinstruction['content']);
				
				$("#popover").css( "display", "none");

				$("#lightbox").css( "display", "block");
				livefire_darken(false, true);
				
				if(currentinstruction['slidein']){
					startsuggestion = $("#popover").outerHeight();
				}
				
				
				var suggestion = $(window).scrollTop() 
												+ ($(window).height()/2) - ($("#popover").outerHeight()/2);
				if(suggestion<20){
					suggestion = 20;
				}
				
				var widthsuggestion = /*$(window).scrollWidth() +*/ ($(window).width()/2) - ($("#popover").outerWidth()/2);
				if(widthsuggestion < 20 ){
					widthsuggestion = 20;
				}
				
				$("#popover").css( "left",widthsuggestion+"px");
				
				if(currentinstruction['slidein']){
					$("#popover").css( "top",-startsuggestion+"px");
					$("#popover").css( "display", "block");				
					$( "#popover" ).animate({ top: [20, "easeInOutQuart"] }, 1500);
				}else{	
					$("#popover").css( "top",suggestion+"px");
					$("#popover").fadeTo(250, 1);
				}
				
				
				break;

			case 'livefire_hidepop':
				livefire_killlightbox();
				break;
			case 'livefire_switchfocus':	
				$("#"+currentinstruction['target']).focus();
				break;		
			case 'livefire_snapin':	
				$("#"+currentinstruction['target']).html(currentinstruction['content']);
				break;		
			case 'livefire_replace':		
				$( "#"+currentinstruction['target'] ).replaceWith(currentinstruction['content']);
				break;
			case 'livefire_addafter':	
				$(currentinstruction['content']).insertAfter( "#"+currentinstruction['target'] );
				break;				
			case 'livefire_addbefore':	
				$(currentinstruction['content']).insertBefore( "#"+currentinstruction['target'] );	
				break;	
			case 'livefire_prepend':	
				$(currentinstruction['content']).prepend( "#"+currentinstruction['target'] );
				break;		
				
			case 'livefire_slidekill':	
				$("#"+currentinstruction['target']).slideUp();
				$("#"+currentinstruction['target']).slideUp( "slow", function() {
				 	// Animation complete.
				 	$("#"+currentinstruction['target']).remove();
  				});
				break;			
			case 'livefire_slidecreate':	
				alert(currentinstruction['message']);
				//$(currentinstruction['content']).prepend( "#"+currentinstruction['target'] );
				break;		
					
			case 'livefire_reveal':		
				$( ".livefirehidden").slideDown();
				//$( ".livefirehidden").removeClass('livefirehidden');
				break;
			case 'livefire_addclass':		
				$( "#"+currentinstruction['target'] ).addClass(currentinstruction['class']);
				break;
			case 'livefire_removeclass':		
				$( "#"+currentinstruction['target'] ).removeClass(currentinstruction['class']);
				break;		
			case 'livefire_urlpush':	
				//alert(currentinstruction['page']);
				history.pushState({}, '', currentinstruction['page']);
				break;		
			case 'livefire_newwindow':	
				//alert(currentinstruction['page']);
				window.open(currentinstruction['page']);
				break;				
			case 'livefire_tiny':	
				//alert(currentinstruction['target']);
				tinymce.EditorManager.editors = []; 
				tinymce.init({
					
					mode : "exact",
					//elements : "copy-edit_copyedit",
					selector: '.copyedit',
					
					entity_encoding : "raw",
					
					forced_root_block : false, 
					plugins: "code, paste, table, link, image",	//table, image and link added by Daniel. Hope they don't come back to haunt me :-/
					paste_as_text: true,

					//----following added by Daniel for image upload support----------------
					
					// enable title field in the Image dialog
					  image_title: true, 
					  // enable automatic uploads of images represented by blob or data URIs
					  automatic_uploads: true,
					  // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
					   images_upload_url: '/inspire_shared/images/tinymce_upload_handler.php',
					  
					  images_upload_base_path: '/inspire_shared/images',
					  
					  
					  // here we add custom filepicker only to Image dialog
					  file_picker_types: 'image', 
					  // and here's our custom image picker
					  file_picker_callback: function(cb, value, meta) {
					    var input = document.createElement('input');
					    input.setAttribute('type', 'file');
					    input.setAttribute('accept', 'image/*');
					    
					    // Note: In modern browsers input[type="file"] is functional without 
					    // even adding it to the DOM, but that might not be the case in some older
					    // or quirky browsers like IE, so you might want to add it to the DOM
					    // just in case, and visually hide it. And do not forget do remove it
					    // once you do not need it anymore.

					    input.onchange = function() {
					      var file = this.files[0];
					      
					      var reader = new FileReader();
					      reader.onload = function () {
					        // Note: Now we need to register the blob in TinyMCEs image blob
					        // registry. In the next release this part hopefully won't be
					        // necessary, as we are looking to handle it internally.
					        var id = 'blobid' + (new Date()).getTime();
					        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
					        var base64 = reader.result.split(',')[1];
					        var blobInfo = blobCache.create(id, file, base64);
					        blobCache.add(blobInfo);

					        // call the callback and populate the Title field with the file name
					        cb(blobInfo.blobUri(), { title: file.name });
					      };
					      reader.readAsDataURL(file);
					    };
					    
					    input.click();
					  }


					});

				break;	
			
			case 'livefire_ajaxform':
				lf('goform',{'form':currentinstruction['form']});	
				break;
				
			case 'livefire_goform':
				//lf('goform', {'form':currentinstruction['form']});
				 window.setTimeout(function() { document.getElementById(currentinstruction['form']).submit(); }, 1000);
				
				break;
					
			case 'livefire_formlive':
				//*
				$("#"+currentinstruction['target']).submit(function(event) {					
					event.preventDefault();
					lf('goform',{'form':$(this).attr('id')});
					return false;
				});//*/
		}
	}
}


function livefire_filemod(event){
	//var filename = ;
	//alert(event.target.value.lastIndexOf("\\") + 1);
	var parent = $(event.target).parent();
	parent.attr('filename',event.target.value.substr(event.target.value.lastIndexOf("\\") + 1));
}


function livefire_darken(target, fade){

	document.getSelection().removeAllRanges();
	
	
	if(target){
		
		$("#lightbox-shade").css( "display", "none");
		//$("#lightbox-highlight-shade").css( "display", "block");
		if(fade){
			$("#lightbox-highlight-shade").fadeTo(250, .4);
		}else{
			$("#lightbox-highlight-shade").css( "opacity", ".4");
			$("#lightbox-highlight-shade").css( "display", "block");
			
		}

		var position = $('#'+target).offset();
		
		
		elementleft = position.left;
		elementtop = position.top;
		elementwidth = $('#'+target).outerWidth(); 
		elementheight = $('#'+target).outerHeight();
		
		fullheight = $(document).outerHeight();
		fullwidth = $(document).outerWidth();
		
		//alert (elementleft+' - '+elementwidth+" | "+elementtop+ " - "+elementheight);
		
		$("#lightbox-shade-l").css( "top", "0px");
		$("#lightbox-shade-l").css( "left", "0px");
		$("#lightbox-shade-l").css( "height", fullheight+"px");
		$("#lightbox-shade-l").css( "width", elementleft+"px");
		
		$("#lightbox-shade-r").css( "top", "0px");
		$("#lightbox-shade-r").css( "left", elementleft+elementwidth+"px");
		$("#lightbox-shade-r").css( "height", fullheight+"px");
		$("#lightbox-shade-r").css( "width", fullwidth-(elementleft+elementwidth) +"px");
		
		$("#lightbox-shade-t").css( "top", "0px");
		$("#lightbox-shade-t").css( "left", elementleft+"px");
		$("#lightbox-shade-t").css( "height", elementtop+"px");
		$("#lightbox-shade-t").css( "width", elementwidth +"px");

		$("#lightbox-shade-b").css( "top", (elementtop+elementheight)+"px");
		$("#lightbox-shade-b").css( "left", elementleft+"px");
		$("#lightbox-shade-b").css( "height", fullheight-(elementtop+elementheight)+"px");
		$("#lightbox-shade-b").css( "width", elementwidth +"px");		
		
		/*$("#lightbox-highlight").css( "display", "block");
		$("#lightbox-highlight").css( "position", "absolute");
		$("#lightbox-highlight").css( "background-color", "red");
		$("#lightbox-highlight").css( "top", elementtop+"px");
		$("#lightbox-highlight").css( "left", elementleft+"px");
		$("#lightbox-highlight").css( "height", elementheight+"px");
		$("#lightbox-highlight").css( "width", elementwidth+"px");*/
	}else{
				
		if(fade){			
			$("#lightbox-shade").fadeTo(250, .4);
		}else{
			$("#lightbox-shade").css( "opacity", ".4");
			$("#lightbox-shade").css( "display", "block");			
		}
		
		$("#lightbox-highlight-shade").css( "display", "none");
	
	}
}


//livefire raget toggling, possibly do via some kind of html attribute,
//we need on change hiding and showing.
//you need a group and an individual.
//so only 1 thing in a group can show at a time
//but you can have multiple groups

function livefire_toggle_between(event){
	var target = $( event.target );	

	$('input[name="'+target.attr('name')+'"]').each(function() {
		area_target = $(this).attr('value');

		if($(this).is(':checked')){
			$('#'+area_target).slideDown();
		}else{
			$('#'+area_target).slideUp();
		}
	});

}


function livefire_toggle_reveal(event){
	var target = $( event.target )
	
	area_target = target.attr('id').substring(0, target.attr('id').length-8);
	label = $('label[for='+target.attr('id')+']');
	//alert(area_target);
	
	if($( target).is(':checked')){
		//mod the label
		label.addClass( "active" );
		//grow the field
		$('#'+area_target).slideDown();
	}else{
		//mod the label		
		label.removeClass( "active" );
		//shrink the field
		$('#'+area_target).slideUp();
	}
}


function livefire_killlightbox(){

	if($("#lightbox-highlight-shade").css( "display") == "block"){
		target = "lightbox-highlight-shade";
	}else{
		target = "lightbox-shade";
	}
	
	$( '#popover' ).fadeOut( 250, function() {
		$("#popover").css( "display", "none");
	});
	
	$( '#'+target ).fadeOut( 250, function() {
		$("#lightbox").css( "display", "none");
	});
	
}
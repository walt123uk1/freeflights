/*jquery.select_skin.js */
/*
 * jQuery select element skinning
 * version: 1.0.7 (11/07/2012)
 * @requires: jQuery v1.2 or later
 * adapted from Derek Harvey code
 *   http://www.lotsofcode.com/javascript-and-ajax/jquery-select-box-skin.htm
 * Licensed under the GPL license:
 *   http://www.gnu.org/licenses/gpl.html
 *
 * Copyright 2010 Colin Verot
 */
 /*exports.getCssStyle = function (ruleSelector, cssprop) {
					for (var c = 0, lenC = document.styleSheets.length; c < lenC; c++) {
					    var rules = document.styleSheets[c].cssRules;
					    for (var r = 0, lenR = rules.length; r < lenR; r++) {
					        var rule = rules[r];
					        if (rule.selectorText == ruleSelector && rule.style) {
					            return rule.style[cssprop]; // rule.cssText;
					        }
					    }
					}
					return null;
				};*/


/*
	
	*/

(function ($) {

    // skin the select
    $.fn.select_skin = function (w) {
        return $(this).each(function(i) {
            s = $(this);
            if (!s.attr('multiple')) {
                // create the container
                s.wrap('<div class="select_skin"></div>');
                c = s.parent();
                c.children().before('<div class="select_skin_text"> </div>').each(function() {
                    if (this.selectedIndex >= 0) $(this).prev().text(this.options[this.selectedIndex].innerText)
                });
                
                c.width(getHiddenOffsetWidth(s) /*s.outerWidth()/*-s.css('padding-left')-s.css('padding-right')*/);
                //c.width(s.css( "width" ));
                


				//ruleSelector, cssprop
				
                if(s.css( "width" )=="0px"){
	               // c.css( "width" ) = getHiddenOffsetWidth(s) + "px";
                	/*c.width( "300px");*/
                	/*
                	var rules = document.styleSheets[0].rules || document.styleSheets[0].cssRules;
					for (var i=0; i < rules.length; i++) {
						var rule = rules[i];
						if (rule.selectorText.toLowerCase() == '#'+s.attr('id')) {
							alert(rule.style.getPropertyValue("width"));
						}
					}
                	
                	
                	//
                	/*for (var d = 0, lenC = document.styleSheets.length; d < lenC; d++) {

					    rules = document.styleSheets[d].rules || document.styleSheets[d].cssRules;
					    //for (var r = 0, lenR = rules.length; r < lenR; r++) {
					        /*var rule = rules[r];
					        if (rule.selectorText == '#'+s.attr('id') && rule.style) {
					            c.width( rule.style["width"]); // rule.cssText;
					        }* /
					    //}
					}*/
					
                }
                //}else{
	                //c.css( "width" ) = "auto";
                //}
                /*
                new_width = exports.getCssStyle('#'+s.attr('id'),"width");
                alert(new_width);         
                c.width(new_width);
                
                
                /*
                var rules = document.styleSheets[0].rules || document.styleSheets[0].cssRules;
               // alert('#'+s.attr('id'));
				for (var i=0; i < rules.length; i++) {
					var rule = rules[i];
					alert(rule.selectorText.toLowerCase().trim());
					if (rule.selectorText.toLowerCase().trim() == '#'+s.attr('id')) {
					alert(rule.style.getPropertyValue("width"));
						c.width(40+rule.style.getPropertyValue("width"));
					}
				}*/
                
                
                c.height(34);
                c.width(s.outerWidth()-2);
                //c.height(s.outerHeight()-2);

                // skin the container
                /*
                c.css('background-color', s.css('background-color'));
                c.css('color', s.css('color'));
                c.css('font-size', s.css('font-size'));
                c.css('font-family', s.css('font-family'));
                c.css('font-style', s.css('font-style'));
                c.css('position', 'relative');
                c.css('height', s.css('height'));
                c.css('line-height', s.css('line-height'));
				
				c.css('margin-bottom', s.css('margin-bottom'));
				c.css('margin-left', s.css('margin-left'));
				c.css('margin-right', s.css('margin-right'));//*/
				//c.css('padding', s.css('padding'));
                // hide the original select
                s.css( { 'opacity': 0 } );

                // get and skin the text label
                var t = c.children().prev();
                //t.width(s.css( "width" ));
                
                
                
                //t.height(c.outerHeight()-s.css('padding-top').replace(/px,*\)*/g,"")-s.css('padding-bottom').replace(/px,*\)*/g,"")-t.css('padding-top').replace(/px,*\)*/g,"")-t.css('padding-bottom').replace(/px,*\)*/g,"")-2);
                //t.width(c.innerWidth()-s.css('padding-right').replace(/px,*\)*/g,"")-s.css('padding-left').replace(/px,*\)*/g,"")-t.css('padding-right').replace(/px,*\)*/g,"")-t.css('padding-left').replace(/px,*\)*/g,"")-c.innerHeight());
                //t.css( { 'opacity': 100, 'overflow': 'hidden', 'position': 'absolute', 'text-indent': '0px', 'z-index': 1, 'top': 0, 'left': 0 } );

                // add events
                c.children().click(function() {
                    //t.html( (this.options.length > 0 && this.selectedIndex >= 0 ? this.options[this.selectedIndex].innerHTML : '') );
                });
                c.children().change(function() {
                    t.html( (this.options.length > 0 && this.selectedIndex >= 0 ? this.options[this.selectedIndex].innerText : '') );
                });
             }
        });
    }

    // un-skin the select
    $.fn.select_unskin = function (w) {
        return $(this).each(function(i) {
            s = $(this);
            if (!s.attr('multiple') && s.parent().hasClass('select_skin')) {
                s.siblings('.select_skin_text').remove();
                s.css( { 'opacity': 100, 'z-index': 0 } ).unwrap();
             }
        });
    }
    
   
	
}(jQuery));

 var getHiddenOffsetWidth = function (el) {
    	// save a reference to a cloned element that can be measured
    	var $hiddenElement = $(el).clone().appendTo('body');
		
    	// calculate the width of the clone
    	var width = $hiddenElement.outerWidth();
		
    	// remove the clone from the DOM
    	$hiddenElement.remove();
		
    	return width;
	};

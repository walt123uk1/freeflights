<?php
	// Declare this as css
	header("Content-type: text/css");

	// Begin the Session	
	session_start();
	if(isset($_SESSION['VARS_EXCHANGE']['highlight'])){
		$colour = $_SESSION['VARS_EXCHANGE']['highlight'] 
	
?>

p a, 
label a,
h3 a,
form#form-lang-switch button.selected label,

body > ul.navigation li.selected a,
table.offer_view_table th:not(.normal),
div.response_terms table th,
.content *.palettecol, 
div#footer ul li.selected a{ 
	color:<?php echo $colour ?>; 
}

form#form-lang-switch button.selected img,
form.responsive a.button_control,
form.responsive label.file:after, 
form.responsive label.date:after,
form.responsive label.reveal_control{ 
	border-color: <?php echo $colour ?>;
}

form#form-lang-switch span.indicator,
form.responsive div.chain a.current, 
form.responsive div.chain a.completed,
form.cookiebanner,
form.responsive label.file:after, 
form.responsive label.date:after,
form.responsive a.style_button, 
form.responsive button.style_button,
form.responsive progress[value],
form.responsive input[type="checkbox"]:checked + label:after{
	background-color: <?php echo $colour ?>;
}

<?php 
	}
?>